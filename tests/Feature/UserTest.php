<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Http\Controllers\Authentication\LoginController;
use App\Http\Controllers\Operations\PlatformSettings\PlatformSettingsController;
use App\Http\Models\Users\User;
use App\Models\Rides\Ride;
use Auth;

class UserTest extends TestCase
{
    
	use DatabaseMigrations;

   	/**
     * A test for login
     *
     * @return void
     */
   	 public function testLogin()
   	 {	
   	 	$login = new LoginController();

   	 	$user = factory(User::class)->create();
   	 	$found_user = User::find(1);

   	 	$this->assertEquals($found_user->id, $user->id);

   	 	$response = $this->post('/api/admin/login', ['email' => $found_user->email, 'password' => 'secret']);

   	 	$response->assertStatus(200);
   	 }

     public function testUpdateCarCount()
     {
        $setting = new PlatformSettingsController();

        $car = factory(\App\Http\Models\Rides\Ride::class)->create();
        $found_car = Ride::find(1);

        $this->assertEquals($found_car->id, $car->id);

        $response = $this->post('/admin/settings', ['car' => 5]);

        $response->assertStatus(200);
     }
}
