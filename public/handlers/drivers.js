var deleteUrl = "";

function setDeleteUrl(url) {
    deleteUrl = url;
}



$(document).on('click', '.swal-button', function (e) {
    location.reload();
});

function readURL(input, holder) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(holder).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#image").change(function () {
    readURL(this , '#driverImage');
});

$("#imageEdit").change(function () {
    readURL(this, '#driverImageEdit');
});

function doDelete() {

    $(".cancel-button").attr("disabled", "disabled");
    $(".confirm-button").attr("disabled", "disabled");
    $('.confirm-button').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');


    simplereq(deleteUrl, function (res, xhr) {
        console.log(res)
        $('.confirm-button').html('Yes,Please').removeAttr("disabled");
        $('.cancel-button').removeAttr("disabled");

        if (xhr.status == 200) {
            $('#reject-book').modal('hide');
            swal("Driver Removed!", "Driver removed successfully", "success")
        }
        else {
            $('#message').html(res.message).show().delay(8000).fadeOut();
        }
    });
}

function pullDetails(url, id, uploadPath){
    $(".btn-edit").attr("disabled", "disabled");
    $('.btn-edit').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');
    
  
    simplereq(url+"/admin/drivers/get"+"/"+id, function (res, xhr) {
        console.log(res)
        $('.btn-edit').html('Edit').removeAttr("disabled");
        $('.btn-edit').removeAttr("disabled");

        if (xhr.status == 200) {
           jsonData = JSON.parse(res.extraMessage)
           //console.log(jsonData.id);
           $('#editFirstName').val(jsonData.firstname)
           $('#editLastName').val(jsonData.lastname)
           $('#editEmail').val(jsonData.email)
           $('#editPhone').val(jsonData.phone)
           $("#editForm").attr('action', res.message+"/"+jsonData.id);
           $("#editImage").attr('src', uploadPath+jsonData.photo);

           $('#edit-book').modal('show');

        }
        else {
            $('#message').html(res.message).show().delay(8000).fadeOut();
        }
    });
}

function updateDriversList(url, id) {
    $(".actionButton").attr("disabled", "disabled");
    $('.actionButton').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');


    simplereq(url + "/admin/drivers/getAvailable" + "/" + id, function (res, xhr) {
        console.log(res)
        $('.actionButton').html('Actions').removeAttr("disabled");
        $('.actionButton').removeAttr("disabled");

        if (xhr.status == 200) {
            jsonData = JSON.parse(res.extraMessage)
            console.log(jsonData)
            for (var i = 0; i < jsonData.length; i++) {
            
                var obj = jsonData[i];
                $('.driverOptions').append('<option value='+obj.id+'>'+obj.firstname+' '+obj.lastname+'</option>');
               
            }

            $(".assignDriverForm").attr('action', res.message);           
            $('#assign-driver').modal('show');
        }
        else {
            $('#message').html(res.message).show().delay(8000).fadeOut();
        }
    });
}

$(document).on('submit', '.assignDriverForm', function (e) {
    e.preventDefault();

    var empty = $(this).find("input").filter(function () {
        if ((this.value == "")) {
            $(this).addClass('error')
        }

        return this.value === "";
    });

    if (!empty.length) {
        $(".submit-button").attr("disabled", "disabled");
        $('.submit-button').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');

        simplesend($(this), $(this).attr('method'), $(this).serialize(), function (res, xhr) {
            $('.submit-button').html('Assign').removeAttr("disabled");
            $('.cancel-button').removeAttr("disabled");

            if (xhr.status == 200) {
                $('#assign-driver').modal('hide');
                swal("Booking Record Updated!", "Driver has been assigned successfully", "success")
            }
            else {
                $('#responses').html(' <div class="alert alert-danger">' + res.message + '. </div> <p style="color:red">').show().delay(8000).fadeOut();
            }
        });
    }
});

$(document).on('submit', '.driverEdit', function (e) {
    e.preventDefault();

    var empty = $(this).find("input").filter(function () {
        if (this.id != 'imageEdit') {
            if ((this.value == "")) {
                $(this).addClass('error')
            }

            return this.value === "";
        }   
    });

    if (!empty.length) {
        $(".submit-button").attr("disabled", "disabled");
        $('.submit-button').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');

        var form = $('#editForm')[0];
        var formData = new FormData(form);

        simplesendWithUpload($(this), $(this).attr('method'), formData, function (res, xhr) {
            $('.submit-button').html('Edit Record').removeAttr("disabled");
            $('.cancel-button').removeAttr("disabled");

            if (xhr.status == 200) {
                $('#new-book').modal('hide');
                swal("Driver Record Updated!", "Driver has been updated successfully", "success")
            }
            else {
                $('#responses').html(' <div class="alert alert-danger">' + res.message + '. </div> <p style="color:red">').show().delay(8000).fadeOut();
            }
        });
    }
});

$(document).on('submit', '.driverCreate', function (e) {
    e.preventDefault();
    
    var empty = $(this).find("input").filter(function () {
        if (this.id != 'image') {
            if ((this.value == "")) {
                $(this).addClass('error')
            }

            return this.value === "";
        }
    });

    if (!empty.length) {
        $(".submit-button").attr("disabled", "disabled");
        $('.submit-button').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');

        var form = $('#createForm')[0];
        var formData = new FormData(form);

        simplesendWithUpload($(this), $(this).attr('method'), formData, function (res, xhr) { 
           $('.submit-button').html('Add Record').removeAttr("disabled");
            $('.cancel-button').removeAttr("disabled");

            if (xhr.status == 200) {
                $('#new-book').modal('hide');
                swal("Driver Added!", "Driver has been added successfully", "success")
            }
            else {
                $('#responses').html(' <div class="alert alert-danger">' + res.message + '. </div> <p style="color:red">').show().delay(8000).fadeOut();
            }
        });
    }
});