
var acceptUrl = "";
var cancelUrl = "";
var completeUrl = "";
var rejectUrl = "";
var baseUrl = "";
var mode = "";

$("input").keyup(function () {
    $(this).removeClass('error')
});

function setConfirmUrl(url) {
    baseUrl = url; 
}

function setAcceptUrl(url){
    acceptUrl = url;
}

function setCancelUrl(url){
    cancelUrl = url;
}

function setCompleteUrl(url){
    completeUrl = url;
}

function setRejectUrl(url) {
    rejectUrl = url;
}

$(document).on('click', '.swal-button', function (e) {
    location.reload();
});

function doConfirm() {
    confirmUrl = encodeURI(baseUrl + '?mode=' + $('#mode').val() + '&tref=' + $('#tref').val());
    
    $(".cancel-button").attr("disabled", "disabled");
    $(".confirm-button").attr("disabled", "disabled");
    $('.confirm-button').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');
   
    
    simplereq(confirmUrl, function (res, xhr) {
        console.log(res)
        $('.confirm-button').html('Yes,Please').removeAttr("disabled");
        $('.cancel-button').removeAttr("disabled");

        if (xhr.status == 200) {
            $('#confirm-book').modal('hide');
            swal("Booking Confirmed!", "A notification mail has been sent to customer", "success")
        }
        else {
            $('#message').html(res.message).show().delay(8000).fadeOut();
       }
    });
}


function doAccept(){
    $(".cancel-button").attr("disabled", "disabled");
    $(".accept-button").attr("disabled", "disabled");
    $('.accept-button').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');

    simplereq(acceptUrl, function (res, xhr) {
        $('.accept-button').html('Yes,Please').removeAttr("disabled");
        $('.cancel-button').removeAttr("disabled");

        if (xhr.status == 200) {
            $('#accept-book').modal('hide');
            swal("Booking Accepted!", "A mail containing payment instructions has been sent to customer", "success")
            
        }
        else {
            window.location.href = '/admin/dashboard';
        }
    });
}

function doComplete() {
    $(".cancel-button").attr("disabled", "disabled");
    $(".accept-button").attr("disabled", "disabled");
    $('.accept-button').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');

    simplereq(completeUrl, function (res, xhr) {
        
        $('.accept-button').html('Yes,Please').removeAttr("disabled");
        $('.cancel-button').removeAttr("disabled");

        if (xhr.status == 200) {
            $('#complete-ride').modal('hide');
            swal("Ride Completed!", "Booking information has been updated.", "success")

        }
        else {
            //window.location.href = '/admin/dashboard';
        }
    });
}



function doReject() {
    $(".cancel-button").attr("disabled", "disabled");
    $(".reject-button").attr("disabled", "disabled");
    $('.reject-button').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');

    simplereq(rejectUrl, function (res, xhr) {
        $('.reject-button').html('Yes,Please').removeAttr("disabled");
        $('.cancel-button').removeAttr("disabled");

        if (xhr.status == 200) {
            $('#reject-book').modal('hide');
            swal("Booking Rejected!", "A mail notifying the customer of the rejection has been sent", "success")
        }
        else {
            window.location.href = '/admin/dashboard';
        }
    }); 
}

function doCancel() {
    $(".cancel-button").attr("disabled", "disabled");
    $(".reject-button").attr("disabled", "disabled");
    $('.reject-button').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');

    simplereq(cancelUrl, function (res, xhr) {
        $('.reject-button').html('Yes,Please').removeAttr("disabled");
        $('.cancel-button').removeAttr("disabled");

        if (xhr.status == 200) {
            $('#reject-book').modal('hide');
            swal("Booking Cancelled!", res.message, "success")
        }
        else {
            window.location.href = '/admin/dashboard';
        }
    });
}

$(document).on('submit', '.bookingCreate', function (e) {
    e.preventDefault();

    var empty = $(this).find("input").filter(function () {
        if ((this.value == "")) {
            $(this).addClass('error')
        }

        return this.value === "";
    });

    if (!empty.length) {
        $(".submit-button").attr("disabled", "disabled");
        $('.submit-button').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');

        simplesend($(this), $(this).attr('method'), $(this).serialize(), function (res, xhr) {
            $('.submit-button').html('Create Booking').removeAttr("disabled");
            $('.cancel-button').removeAttr("disabled");

            if (xhr.status == 200) {
                $('#new-book').modal('hide');
                swal("Booking Created!", "Booking has been created successfully", "success")
            }
            else {
                $('#responses').html(' <div class="alert alert-danger">' + res.message + '. </div> <p style="color:red">').show().delay(8000).fadeOut();
               }
        });
    }
});