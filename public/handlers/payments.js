var paidUrl = "";

function setPaidUrl(url) {
    paidUrl = url;
}

function markAsPaid() {
  
    $(".accept-button").attr("disabled", "disabled");
    $(".accept-button").attr("disabled", "disabled");
    $('.accept-button').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');


    simplereq(paidUrl, function (res, xhr) {
        console.log(res)
        $('.accept-button').html('Yes,Please').removeAttr("disabled");
        $('.accept-button').removeAttr("disabled");

        if (xhr.status == 200) {
            $('#confirm-refund').modal('hide');
            swal("Refund Confirmed!", "Refund has been confirmed and a notification has been sent to the customer", "success")
        }
        else {
            $('#message').html(res.message).show().delay(8000).fadeOut();
        }
    });
}

$(document).on('click', '.swal-button', function (e) {
    location.reload();
});
