$(function () {
    $("button").prop("disabled", false);
    //console.log(document.location.hostname)
});     

$(document).on('submit', '.adminLogin', function (e) {
    e.preventDefault();

    var empty = $(this).find("input").filter(function () {
        if ((this.value == "")) {
            $(this).addClass('error')
        }

        return this.value === "";
    });

    if (!empty.length) {
        $(".submit-button").attr("disabled", "disabled");
        $('.submit-button').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');

        simplesend($(this), $(this).attr('method'), $(this).serialize(), function (res , xhr) {
            //console.log(res)
          
            if (xhr.status != 200) {
                //$('html, body').animate({scrollTop: '0px'}, 300);
                $('#responses').html(' <div class="alert alert-danger">' + res.extraMessage+'. </div> <p style="color:red">').show().delay(8000).fadeOut();
                $('.submit-button').html('LOG IN').removeAttr("disabled");

                return;
            }
            else {
                window.location.href = '/admin/dashboard';
            }
        });
    }
});

$(document).on('submit', '.adminRecover', function (e) {
    e.preventDefault();

    var empty = $(this).find("input").filter(function () {
        if ((this.value == "")) {
            $(this).addClass('error')
        }

        return this.value === "";
    });

    if (!empty.length) {
        $(".recover-button").attr("disabled", "disabled");
        $('.recover-button').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');

        
        simplesend($(this), $(this).attr('method'), $(this).serialize(), function (res, xhr) {
            console.log(res)

            if (xhr.status != 200) {
                //$('html, body').animate({scrollTop: '0px'}, 300);
                $('#responses2').html(' <div class="alert alert-danger">' + res.extraMessage + '. </div> <p style="color:red">').show().delay(8000).fadeOut();
                $('.recover-button').html('RESET').removeAttr("disabled");
                return;
            }
            else {
                window.location.href = '/admin/reset-notification'
            }
        });
    }
});

$(document).on('submit', '.adminReset', function (e) {
    e.preventDefault();

    var empty = $(this).find("input").filter(function () {
        if ((this.value == "")) {
            $(this).addClass('error')
        }

        return this.value === "";
    });

    //confirm passwords
    if($('#password').val() !== $('#passwordconfirm').val()){
        $('#responses').html(' <div class="alert alert-danger">Passwords do not match. </div> <p style="color:red">').show().delay(8000).fadeOut();
        return;
    }

    if (!empty.length) {
        $(".reset-button").attr("disabled", "disabled");
        $('.reset-button').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');


        simplesend($(this), $(this).attr('method'), $(this).serialize(), function (res, xhr) {
            console.log(res)

            if (xhr.status != 200) {
                //$('html, body').animate({scrollTop: '0px'}, 300);
                $('#responses').html(' <div class="alert alert-danger">' + res.extraMessage + '. </div> <p style="color:red">').show().delay(8000).fadeOut();
                $('.reset-button').html('RESET PASSWORD').removeAttr("disabled");
                return;
            }
            else {
                window.location.href = '/admin/success-notification'
            }
        });
    }
});
