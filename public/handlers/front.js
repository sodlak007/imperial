$(document).on('submit', '.bookingCreate', function (e) {
    e.preventDefault();

    var empty = $(this).find("input,textarea").filter(function () {
        if ((this.value == "")) {
            var name = $(this).attr('name')
            $("span").remove("." + name);
            $('#' + name).addClass('form-error').append('<span class="'+name+'">Field is required</span>')
        }

        return this.value === "";
    });

    if (!empty.length) {
        $(".submit-button").attr("disabled", "disabled");
        $('.submit-button').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');

        simplesend($(this), $(this).attr('method'), $(this).serialize(), function (res, xhr) {
            console.log(res)

            if (xhr.status != 200) {
                //$('html, body').animate({scrollTop: '0px'}, 300);
                $('#responses').html('<div class="alert alert-danger">' + res.message + '. </div> <p style="color:red">').show().delay(8000).fadeOut();
                $('.submit-button').html('BOOK RIDE').removeAttr("disabled");

                return;
            }
            else {
                //empty the form
                $('.submit-button').html('BOOK RIDE').removeAttr("disabled");
                $('#createForm')[0].reset();
                //show success modal
                $('#request-modal').fadeOut();
                $('#success-modal').fadeIn();
            }
        });
    }
});

$(document).on('submit', '.userCancel', function (e) {
    e.preventDefault();

    var empty = $(this).find("input,textarea").filter(function () {
        if ((this.value == "")) {
            var name = $(this).attr('name')
            $("span").remove("." + name);
            $('#' + name).addClass('form-error').append('<span class="' + name + '">Field is required</span>')
        }

        return this.value === "";
    });

    if (!empty.length) {
        $(".submit-button").attr("disabled", "disabled");
        $('.submit-button').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');

        simplesend($(this), $(this).attr('method'), $(this).serialize(), function (res, xhr) {
            console.log(res)

            if (xhr.status != 200) {
                //$('html, body').animate({scrollTop: '0px'}, 300);
                $('#responses').html('<div class="alert alert-danger">' + res.message + '. </div> <p style="color:red">').show().delay(8000).fadeOut();
                $('.submit-button').html('CANCEL RIDE').removeAttr("disabled");

                return;
            }
            else {
                //empty the form and go to success page
                $('.submit-button').html('CANCEL RIDE').removeAttr("disabled");
                $('#userCancelForm')[0].reset();
                window.location.href = '/ride/cancel-success';
            }
        });
    }
});

$(document).on('click', '.swal-button', function (e) {
    location.reload();
});

$("input").keyup(function () {
    var name = $(this).attr('name')
    $('#' + name).removeClass('form-error')
    $("span").remove("."+name);
});