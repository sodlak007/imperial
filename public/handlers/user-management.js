var editUrl = "";
var deleteUrl = ""
var baseUrl = ""
var mode = "";

function setEditUrl(url) {
    $("#editForm").attr('action', url);
}

function setDeleteUrl(url) {
    deleteUrl = url;
}

$(document).on('click', '.swal-button', function (e) {
    location.reload();
});

function doDelete() {
    $(".cancel-button").attr("disabled", "disabled");
    $(".reject-button").attr("disabled", "disabled");
    $('.reject-button').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');

    simplereq(deleteUrl, function (res, xhr) {
        $('.reject-button').html('Yes,Please').removeAttr("disabled");
        $('.cancel-button').removeAttr("disabled");

        if (xhr.status == 200) {
            $('#reject-book').modal('hide');
            swal("Delete Complete!", "Admin deleted successfully", "success")
        }
        else {
            window.location.href = '/admin/dashboard';
        }
    });
}

$(document).on('submit', '.editForm', function (e) {
    e.preventDefault();

    
    var empty = $(this).find("input").filter(function () {
        if ((this.value == "")) {
            $(this).addClass('error')
        }

        return this.value === "";
    });

    if (!empty.length) {
        $(".submit-button").attr("disabled", "disabled");
        $('.submit-button').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');

        simplesend($(this), $(this).attr('method'), $(this).serialize(), function (res, xhr) {
            console.log(res)
            $('.submit-button').html('Edit').removeAttr("disabled");
            $('.cancel-button').removeAttr("disabled");

            if (xhr.status == 200) {
                $('#new-book').modal('hide');
                swal("Complete!", "Admin information edited successfully", "success")
            }
            else {
                $('#responses').html(' <div class="alert alert-danger">' + res.message + '. </div> <p style="color:red">').show().delay(8000).fadeOut();
            }
        });
    }
});

$(document).on('submit', '.createForm', function (e) {
    e.preventDefault();


    var empty = $(this).find("input").filter(function () {
        if ((this.value == "")) {
            $(this).addClass('error')
        }

        return this.value === "";
    });

    if (!empty.length) {
        $(".submit-button").attr("disabled", "disabled");
        $('.submit-button').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i><span class="sr-only">Processing...</span>');

        simplesend($(this), $(this).attr('method'), $(this).serialize(), function (res, xhr) {
            console.log(res)
            $('.submit-button').html('Add Admin').removeAttr("disabled");
            $('.cancel-button').removeAttr("disabled");

            if (xhr.status == 200) {
                $('#new-book').modal('hide');
                swal("Complete!", "Admin created successfully", "success")
            }
            else {
                $('#responses').html(' <div class="alert alert-danger">' + res.message + '. </div> <p style="color:red">').show().delay(8000).fadeOut();
            }
        });
    }
});