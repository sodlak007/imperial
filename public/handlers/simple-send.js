//ajax function to make requests
function simplesend(form, type, dataset, callback) {
    var URL = form.attr('action');

    $.ajax({
        url: URL,
        type: type,
        dataType: 'json',
        data: dataset, 

        success: function (response,textStatus, xhr) {
            if(response.status == 206) {
                $('#accept-book').modal('hide');
                swal("Process Incomplete!", "Could not proceed, please check your internet connection", "error")

            }else
              callback(response, xhr);
        },
        error: function () {
            window.location.href = '/admin/dashboard';
        }
    });
}

function simplesendWithUpload(form, type, dataset, callback) {
    var URL = form.attr('action');

    $.ajax({
        url: URL,
        type: type,
        processData: false,
        contentType: false,
        dataType: 'json',
        data: dataset,

        success: function (response, textStatus, xhr) {
            if (response.status == 206) {
                $('#accept-book').modal('hide');
                swal("Process Incomplete!", "Could not proceed, please check your internet connection", "error")

            } else
                callback(response, xhr);
        },
        error: function () {
           //window.location.href = '/admin/dashboard';
        }
    });
}

function simplereq(url, callback) {
    
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
      
        success: function (response, textStatus, xhr) {
           if (xhr.status == 206) {
                $('#accept-book').modal('hide');
                swal("Process Incomplete!", "Could not proceed, please check your internet connection", "error")

            }
            else
               callback(response, xhr);
        },
        error: function () {
            window.location.href = '/admin/dashboard';
        }
    });
}