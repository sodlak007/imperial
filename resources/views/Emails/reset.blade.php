<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>project</title>
    <style type="text/css">
        @media only screen {
@font-face {
    font-family: 'Fira Sans';
    font-style: normal;
    font-weight: 400;
    src: local("Fira Sans Regular"), local("FiraSans-Regular"), url(https://fonts.gstatic.com/s/firasans/v8/MIPWVWI_mY_QERxcMVPEwIX0hVgzZQUfRDuZrPvH3D8.woff2) format("woff2");
    unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
}
@font-face {
    font-family: 'Fira Sans';
    font-style: normal;
    font-weight: 400;
    src: local("Fira Sans Regular"), local("FiraSans-Regular"), url(https://fonts.gstatic.com/s/firasans/v8/EjsrzDkQUQCDwsBtLpcVQZBw1xU1rKptJj_0jans920.woff2) format("woff2");
    unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215;
}
@font-face {
    font-family: 'Fira Sans';
    font-style: normal;
    font-weight: 500;
    src: local("Fira Sans Medium"), local("FiraSans-Medium"), url(https://fonts.gstatic.com/s/firasans/v8/zM2u8V3CuPVwAAXFQcDi4IjoYw3YTyktCCer_ilOlhE.woff2) format("woff2");
    unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
}
@font-face {
    font-family: 'Fira Sans';
    font-style: normal;
    font-weight: 500;
    src: local("Fira Sans Medium"), local("FiraSans-Medium"), url(https://fonts.gstatic.com/s/firasans/v8/zM2u8V3CuPVwAAXFQcDi4Bampu5_7CjHW5spxoeN3Vs.woff2) format("woff2");
    unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215;
}
@font-face {
    font-family: 'Fira Sans';
    font-style: normal;
    font-weight: 700;
    src: local("Fira Sans Bold"), local("FiraSans-Bold"), url(https://fonts.gstatic.com/s/firasans/v8/DugPdSljmOTocZOR2CItOojoYw3YTyktCCer_ilOlhE.woff2) format("woff2");
    unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
}
@font-face {
    font-family: 'Fira Sans';
    font-style: normal;
    font-weight: 700;
    src: local("Fira Sans Bold"), local("FiraSans-Bold"), url(https://fonts.gstatic.com/s/firasans/v8/DugPdSljmOTocZOR2CItOhampu5_7CjHW5spxoeN3Vs.woff2) format("woff2");
    unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215;
}
@font-face {
    font-family: 'Fira Sans';
    font-style: normal;
    font-weight: 800;
    src: local("Fira Sans ExtraBold"), local("FiraSans-ExtraBold"), url(https://fonts.gstatic.com/s/firasans/v8/htOw9f-chtELyJuFCkCrFojoYw3YTyktCCer_ilOlhE.woff2) format("woff2");
    unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
}
@font-face {
    font-family: 'Fira Sans';
    font-style: normal;
    font-weight: 800;
    src: local("Fira Sans ExtraBold"), local("FiraSans-ExtraBold"), url(https://fonts.gstatic.com/s/firasans/v8/htOw9f-chtELyJuFCkCrFhampu5_7CjHW5spxoeN3Vs.woff2) format("woff2");
    unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215;
}
}
        @media only screen and (min-width: 621px) {
.container {
    width: 620px !important;
}
}
        @media only screen and (max-width: 620px) {
table[class=body] .menu_style_1 .menu__inner {
    padding: 25px 30px !important;
}

table[class=body] .menu_style_1 .menu__column {
    max-width: 100% !important;
} table[class=body] .section_features-1 .section__box {
    padding-top: 35px !important;
    padding-bottom: 15px !important;
    padding-left: 10px !important;
    padding-right: 10px !important;
}

table[class=body] .features-row_section-1 .features-row__column {
    max-width: 50% !important;
} table[class=body] .section_style_1 .section__box {
    padding: 35px 30px !important;
}
}
        @media only screen and (max-width: 525px) {
.spacing_h_80 td {
    height: 40px !important;
    line-height: 40px !important;
    font-size: 40px !important;
} table[class=body] .menu_style_1 .menu__inner {
    padding: 15px 20px !important;
} table[class=body] .section_features-1 .section__box {
    padding-top: 25px !important;
    padding-bottom: 5px !important;
    padding-left: 0 !important;
    padding-right: 0 !important;
}

table[class=body] .features-row_section-1 .features-row__column {
    max-width: 100% !important;
} table[class=body] .section_style_1 .section__box {
    padding: 25px 20px !important;
}

table[class=body] .section__title br,
table[class=body] .section__description br {
    display: none !important;
}
}
    </style>
</head>
<body style="font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 16px; width: 100% !important; Margin: 0 !important; padding: 0; line-height: 1.5; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; background-color: #f4f4f4;">
<table class="body" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
    <tr>
        <td class="wrapper" align="center" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; padding: 0; vertical-align: top; background-color: #f4f4f4;" valign="top" bgcolor="#f4f4f4">
            <!--[if (gte mso 9)|(IE)]>
            <table width="620" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;">
            <tr>
            <td width="620" align="center" valign="top">
            <![endif]-->
            <table align="center" class="container" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; Margin: 0 auto; max-width: 620px;" width="100%">
                <tr>
                    <td class="content" align="center" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; vertical-align: top; padding: 0 10px;" valign="top">
                        <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;"></span>
                        <table class="spacing spacing_h_20" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                            <tr>
                                <td style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; vertical-align: top; padding: 0; height: 20px; font-size: 20px; line-height: 20px;" valign="top">&nbsp;</td>
                            </tr>
                        </table>

                        <!-- START MODULE: Menu 1 -->
                        <table class="menu menu_style_1" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                            <tr>
                                <td class="menu__inner" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; vertical-align: top; padding: 29px 40px; background-color: #003C93; font-size: 0; text-align: center;" valign="top" bgcolor="#1B1B1B" align="center">
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;">
                                        <tr>
                                            <td width="102" valign="top">
                                    <![endif]-->
                                   <!--  <div class="menu__column menu__column_socials" style="box-sizing: border-box; display: inline-block; width: 100%; vertical-align: top; padding: 10px 0; max-width: 102px;">
                                        <table style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                                            <tr>
                                                <td align="center" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; padding: 0; vertical-align: top;" valign="top">
                                                    <table class="menu__socials socials socials_light socials_style_1 socials_menu-1" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;">
                                                        <tr>
                                                            <td class="socials__item" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; padding: 0; vertical-align: middle; font-size: 0; line-height: 0; padding-top: 4px; padding-right: 12px;" valign="middle">
                                                                <a class="socials__link" href="http://example.com" target="_blank" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; color: #1595E7; text-decoration: none; border: 0; outline: 0;">
                                                                    <img class="socials__icon" src="images/cbe0e7f9-a66f-49df-bd19-6f108d21b8e3.png" width="15" height="15" alt="Facebook" style="outline: none; max-width: 100%; border: 0; Margin: 0; text-decoration: none; line-height: 100%; height: auto; -ms-interpolation-mode: bicubic; display: block; font-size: 14px; font-weight: 700; color: #ffffff;">
                                                                </a>
                                                            </td>
                                                            <td class="socials__item" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; padding: 0; vertical-align: middle; font-size: 0; line-height: 0; padding-top: 4px; padding-right: 12px;" valign="middle">
                                                                <a class="socials__link" href="http://example.com" target="_blank" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; color: #1595E7; text-decoration: none; border: 0; outline: 0;">
                                                                    <img class="socials__icon" src="images/419bee99-c52c-4d62-b256-2cd4b5722616.png" width="16" height="14" alt="Twitter" style="outline: none; max-width: 100%; border: 0; Margin: 0; text-decoration: none; line-height: 100%; height: auto; -ms-interpolation-mode: bicubic; display: block; font-size: 14px; font-weight: 700; color: #ffffff;">
                                                                </a>
                                                            </td>
                                                            <td class="socials__item" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; padding: 0; vertical-align: middle; font-size: 0; line-height: 0; padding-top: 4px; padding-right: 12px;" valign="middle">
                                                                <a class="socials__link" href="http://example.com" target="_blank" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; color: #1595E7; text-decoration: none; border: 0; outline: 0;">
                                                                    <img class="socials__icon" src="images/c8407bf8-9f50-4f94-a486-2d182663f218.png" width="22" height="15" alt="Google Plus" style="outline: none; max-width: 100%; border: 0; Margin: 0; text-decoration: none; line-height: 100%; height: auto; -ms-interpolation-mode: bicubic; display: block; font-size: 14px; font-weight: 700; color: #ffffff;">
                                                                </a>
                                                            </td>
                                                            <td class="socials__item" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; padding: 0; vertical-align: middle; font-size: 0; line-height: 0; padding-top: 4px;" valign="middle">
                                                                <a class="socials__link" href="http://example.com" target="_blank" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; color: #1595E7; text-decoration: none; border: 0; outline: 0;">
                                                                    <img class="socials__icon" src="images/475132ed-2c85-4960-84f2-beb57ee1d395.png" width="16" height="15" alt="Instagram" style="outline: none; max-width: 100%; border: 0; Margin: 0; text-decoration: none; line-height: 100%; height: auto; -ms-interpolation-mode: bicubic; display: block; font-size: 14px; font-weight: 700; color: #ffffff;">
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div> -->
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    <td width="316" valign="top">
                                    <![endif]-->
                                    <div class="menu__column menu__column_logo" style="box-sizing: border-box; display: inline-block; width: 100%; vertical-align: top; padding: 10px 0; max-width: 100%px;">
                                        <table style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                                            <tr>
                                                <td align="center" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; padding: 0; vertical-align: top;" valign="top">
                                                    <table class="menu__logo table-auto" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;">
                                                        <tr>
                                                            <td class="menu__logo-inner" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; padding: 0; vertical-align: top;" valign="top">
                                                                <a class="menu__logo-link" href="http://example.com" target="_blank" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; color: #1595E7; text-decoration: none; border: 0; outline: 0;">
                                                                    <img src="https://res.cloudinary.com/www-leanstack-co/image/upload/v1519335996/logo_imperial.png" class="menu__logo-img menu__logo-img_light" width="147" height="22" alt="Postcards" style="max-width: 100%; height: auto; border: 0; Margin: 0; text-decoration: none; line-height: 100%; outline: none; -ms-interpolation-mode: bicubic; display: block; color: #ffffff;">
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    <td width="102" valign="top">
                                    <![endif]-->
                                    <div class="menu__column menu__column_holder" style="box-sizing: border-box; display: inline-block; width: 100%; vertical-align: top; max-width: 102px;">&nbsp;</div>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </table>
                        <!-- END MODULE: Menu 1 -->



                        <!-- START MODULE: Feature 1 -->
                        <table class="section section_features-1" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                            <tr>
                                <td class="section__box" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; vertical-align: top; padding: 40px; background-color: #ffffff; padding-top: 40px; padding-bottom: 20px; padding-left: 20px; padding-right: 20px;" valign="top" bgcolor="#ffffff">
                                    <table style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                                        <tr>
                                            <td class="section__title" style="-webkit-text-size-adjust: 100%; box-sizing: border-box; -ms-text-size-adjust: 100%; padding: 0; vertical-align: top; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 500; line-height: 1.42; letter-spacing: -.4px; color: #151515; padding-left: 20px; padding-right: 20px;" valign="top">Hi {{$data['name']}},</td>
                                        </tr>
                                        <tr>
                                            <td style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; padding: 0; vertical-align: top;" valign="top">
                                                <table class="spacing spacing_h_10" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                                                    <tr>
                                                        <td style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; vertical-align: top; padding: 0; height: 10px; font-size: 10px; line-height: 10px;" valign="top">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="section__description" style="-webkit-text-size-adjust: 100%; box-sizing: border-box; -ms-text-size-adjust: 100%; padding: 0; vertical-align: top; font-family: 'Fira Sans', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 300; line-height: 1.56; letter-spacing: -.2px; color: #9B9B9B; padding-left: 20px; padding-right: 20px;" valign="top">
                                                You requested that your password be reset on Imperial.  If you do not wish to reset your password, please ignore this message.<br/><br/>
                                                To reset your password, please click the following link, <br/><br/>

                                                <a href="{{$data['message']}}">Click here to reset password</a>

                                                
                                                <br><br>Cheers<br/>
                                                The Imperial Team
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; padding: 0; vertical-align: top;" valign="top">
                                                <table class="spacing spacing_h_20" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                                                    <tr>
                                                        <td style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; vertical-align: top; padding: 0; height: 20px; font-size: 20px; line-height: 20px;" valign="top">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <!-- END MODULE: Feature 1 -->

                                <table class="spacing spacing_h_20" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                                    <tr>
                                        <td style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; box-sizing: border-box; vertical-align: top; padding: 0; height: 20px; font-size: 20px; line-height: 20px;" valign="top">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!--[if (gte mso 9)|(IE)]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
        </table>
    </body>
</html>