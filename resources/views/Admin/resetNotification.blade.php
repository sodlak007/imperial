@include('Admin.Assets.head') 

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register" style="background-image:url('{{URL::asset('images/background/uberlux-london-1050x656.jpg')}}');">
            <div class="login-box card">
                <div class="card-body">
                    
                    <p>Your password reset link has been sent to your email address.</p>
                    <a href="{{url('/admin/login')}}" class="btn  btn-info">Return to Login</a>
                </div>
            </div>
        </div>
    </section>

</body>
@include('Admin.Assets.foot') 
</html>