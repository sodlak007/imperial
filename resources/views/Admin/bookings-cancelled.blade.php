@include('Admin.Assets.head') 
<body class="fix-header fix-sidebar card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        @include('Admin.Layout.header') 
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Cancelled Bookings</h3>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
               
                 @if($total == 0)
                  <div class="card">
                    <div class="card-body collapse show">
                       
                        <p class="card-text all-clear-text">All Clear <span>No bookings at the moment</span></p>
                    </div>
                </div>
                @else
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                
                               <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <!-- <th>#</th> -->
                                                <th>Full Name</th>
                                                <th>Email</th>
                                                <th>Phone No</th>
                                                <th>Pickup Address</th>
                                                <th>Date </th>
                                                <th>Time</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($cancelledBookings as $booking)
                                            <tr>
                                                <!-- <td>{{$booking->id}}</td> -->
                                                <td>{{$booking->name}}</td>
                                                <td>{{$booking->email}}</td>
                                                <td>{{$booking->phone}}</td>
                                                <td>{{$booking->pickUpAddress}}</td>
                                                <td>{{date("F j, Y", strtotime($booking->date))}} </td>
                                                <td>{{date("g:i a", strtotime($booking->time))}} </td>
                                            </tr>
                                            @endforeach
                                          
                                          
                                        </tbody>
                                    </table>

                                    @if($total > 20)
                                    <nav aria-label="Page navigation example">
                                      <ul class="pagination">
                                        @if($_GET['page'] != 1)
                                          <li class="page-item"><a class="page-link" href="{{url('/admin/bookings/cancelled?page=')}}{{$_GET['page'] - 1}}">Previous</a></li>
                                        @else
                                          <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                        @endif

                                       @if((($_GET['page'] * 20) < $total) && ($total != 0))               
                                           <li class="page-item"><a class="page-link" href="{{url('/admin/bookings/cancelled?page=')}}{{$_GET['page'] + 1}}">Next</a></li>
                                        @else
                                          <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                        @endif     
                                      </ul>
                                    </nav>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
              
            </div>

            <div id="complete-ride" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Completed Ride</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <p>Confirm this ride as completed?</p>
                        </div>
                        <div class="modal-footer">
                            <button  type="button" class="btn btn-danger cancel-button" data-dismiss="modal">Cancel</button>
                            <button onclick="doComplete();" type="button" class="btn btn-success accept-button">Yes, Please</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                 <!-- /.modal-dialog -->
              </div>

          
              <div id="cancel-book" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Cancel Booking</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to cancel this booking?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger cancel-button" data-dismiss="modal">Cancel</button>
                            <button onclick="doCancel();" type="button" class="btn btn-success reject-button">Yes, Please</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2018 - Imperial </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    
</body>
@include('Admin.Assets.foot') 
<script src="{{URL::asset('handlers/simple-send.js')}}"></script>
<script src="{{URL::asset('handlers/bookings.js')}}"></script>
</html>