@include('Admin.Assets.head') 
<body class="fix-header fix-sidebar card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
         @include('Admin.Layout.header') 
      
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">User Management</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <button type="button" class="btn waves-effect waves-light btn-info" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#new-book" style="float: right;">New Admin</button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Full Name</th>
                                                <th>Email</th>
                                                <th>Role</th>
                                                <th>Action</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                         
                                         <?php $i = ( ($_GET['page'] - 1) * 20) +  1; ?>
                                          @foreach($data as $datum)
                                            <tr>
                                                <td><a href="javascript:void(0)">{{$i++}}</a></td>
                                                <td>{{$datum->name}}</td>
                                                <td>{{$datum->email}}</td>
                                                <td>{{$datum->role}}</td>

                                                <td>
                                                    <button type="button" onclick="setEditUrl('{{url('/admin/edit/')}}/{{$datum->id}}')" class="btn btn-xs btn-success edit-book" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#edit-book">Edit</button>
                                                    <button type="button" onclick="setDeleteUrl('{{url('/admin/delete/')}}/{{$datum->id}}')" class="btn waves-effect waves-light btn-xs btn-danger" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#reject-book" >Delete</button>
                                                </td>
                                     
                                           </tr>
                                        @endforeach
                                          
                                        </tbody>
                                    </table>

                                    @if($total > 20)
                                    <nav aria-label="Page navigation example">
                                      <ul class="pagination">
                                        @if($_GET['page'] != 1)
                                          <li class="page-item"><a class="page-link" href="{{url('/admin/user-management?page=')}}{{$_GET['page'] - 1}}">Previous</a></li>
                                        @else
                                          <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                        @endif

                                       @if( (($_GET['page'] * 20) < $total) && ($total != 0))               
                                        <li class="page-item"><a class="page-link" href="{{url('/admin/user-management?page=')}}{{$_GET['page'] + 1}}">Next</a></li>
                                        @else
                                          <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                        @endif     
                                      </ul>
                                    </nav>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              
            </div>

            

             <div id="reject-book" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Delete User</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to delete this user?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger cancel-button" data-dismiss="modal">Cancel</button>
                            <button onclick="doDelete();" type="button" class="btn btn-success reject-button">Yes, Please</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            
            <div id="edit-book" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Edit Admin Permissions</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                         {!! Form::open(array('url' => url("/admin/edit"),'class'=>'editForm','id'=>'editForm')) !!}
                            <div class="modal-body">
                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Role</label>
                                        <select name="role" class="form-control custom-select">
                                            <option value="ADMIN">ADMIN</option>
                                            <option value="SUPERADMIN">SUPER ADMIN</option>
                                        </select>
                                    </div>
                                    
                            </div>
                            <div class="modal-footer">
                                <button type="submit"  class="btn btn-info submit-button">Edit</button>
                                <button type="button" class="btn btn-danger cancel-button" data-dismiss="modal">Cancel</button>
                            </div>
                       </form>
                  </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

            <div id="new-book" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">New Booking</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                        {!! Form::open(array('url' => url("/admin/create"),'class'=>'createForm','id'=>'createForm')) !!}
                            <div id="responses">
                                 </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Full Name</label>
                                    <input type="text" class="form-control" name="name" id="recipient-name1">
                                </div> 
                                 <div class="form-group">
                                    <label for="recipient-name" class="control-label">Email</label>
                                    <input type="email" class="form-control" name="email" id="recipient-name1">
                                 </div>
                                 <div class="form-group">
                                    <label for="recipient-name" class="control-label">Role</label>
                                    <select name="role" class="form-control custom-select">
                                        <option value="ADMIN">ADMIN</option>
                                        <option value="SUPERADMIN">SUPER ADMIN</option>
                                    </select>
                                </div>
                                
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info submit-button">Add Admin</button>
                                <button type="button" class="btn btn-danger cancel-button" data-dismiss="modal">Cancel</button>
                            </div>
                     </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2018 - Imperial </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
   @include('Admin.Assets.foot') 
</body>
<script src="{{URL::asset('handlers/simple-send.js')}}"></script>
<script src="{{URL::asset('handlers/user-management.js')}}"></script>

</html>