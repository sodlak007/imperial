@include('Admin.Assets.head') 

<body class="fix-header fix-sidebar card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
           @include('Admin.Layout.header') 
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Drivers</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <button type="button" class="btn waves-effect waves-light btn-info" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#new-book" style="float: right;">Add A Driver</button>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Email</th>
                                                <th>Phone No</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($drivers as $driver)
                                            <tr>
                                                <td>{{$driver->id}}</td>
                                                <td>{{$driver->firstname}}</td>
                                                <td>{{$driver->lastname}}</td>
                                                <td>{{$driver->email}}</td>
                                                <td>{{$driver->phone}}</td>
                                                
                                                <td>
                                                    <button type="button" onclick="pullDetails( '{{url('/')}}' , {{$driver->id}}, '{{env('DRIVER_UPLOADS')}}' );" class="btn btn-xs btn-success btn-edit" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#new-boo">Edit</button>
                                                    <button type="button" onclick="setDeleteUrl('{{url('/admin/driver/delete')}}/{{$driver->id}}')" class="btn waves-effect waves-light btn-xs btn-danger" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#reject-book" >Delete</button>
                                                    <button type="button" onclick="location.href = '{{url('/admin/rides/history/')}}/{{$driver->id}}?page=1'" class="btn waves-effect waves-light btn-xs btn-danger" data-toggle="modal" data-backdrop="static" data-keyboard="false">Ride History</button>
                                                </td>
                                            </tr>

                                          @endforeach
                                        </tbody>
                                    </table>

                                    @if($total > 20)
                                    <nav aria-label="Page navigation example">
                                      <ul class="pagination">
                                        @if($_GET['page'] != 1)
                                          <li class="page-item"><a class="page-link" href="{{url('/admin/drivers?page=')}}{{$_GET['page'] - 1}}">Previous</a></li>
                                        @else
                                          <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                        @endif

                                       @if((($_GET['page'] * 20) < $total) && ($total != 0))               
                                           <li class="page-item"><a class="page-link" href="{{url('/admin/drivers?page=')}}{{$_GET['page'] + 1}}">Next</a></li>
                                        @else
                                          <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                        @endif     
                                      </ul>
                                    </nav>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              
            </div>

            

             <div id="reject-book" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Delete Record</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to delete this driver?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger cancel-button" data-dismiss="modal">Cancel</button>
                            <button type="button" onclick="doDelete();" class="btn btn-success confirm-button">Yes, Please</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

            <div id="new-book" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">New Driver</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            {!! Form::open(array('url' => url("/admin/driver/create"),'class'=>'driverCreate','id'=>'createForm', 'enctype'=>"multipart/form-data")) !!}

                                <div id="responses">
                                    @if (Session::has('message'))
                                    <div class="alert alert-success"> {{ Session::get('message') }} </div>                           
                                    @endif
                                </div>

                                 <div class="form-group">
                                    <label for="recipient-name" class="control-label">First Name</label>
                                    <input type="text" name="driverFirstName" class="form-control" id="recipient-name1">
                                </div>

                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Last Name</label>
                                    <input type="text" name="driverLastName" class="form-control" id="recipient-name1">
                                </div>
                                
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Email</label>
                                    <input type="email" name="driverEmail" class="form-control" id="recipient-name1">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Phone No</label>
                                    <input type="tel" name="driverPhone" class="form-control" id="recipient-name1">
                                </div>

                                <div class="form-group">
                                    <label for="image" class="btn btn-info ">Upload Driver Image</label>
                                    <input type='file' style="visibility:hidden;" name="imageToUpload" id="image" /><br>
                                    <img height="120" width="120" id="driverImage" src="#" alt="your image" />
                                </div>
                                
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-info submit-button">Add Record</button>
                                    <button type="button" class="btn btn-danger cancel-button" data-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                        
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

             <div id="edit-book" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">New Driver</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            {!! Form::open(array('url' => url("/admin/driver/edit"),'class'=>'driverEdit','id'=>'editForm','enctype'=>"multipart/form-data")) !!}

                                <div id="responses">
                                    @if (Session::has('message'))
                                    <div class="alert alert-success"> {{ Session::get('message') }} </div>                           
                                    @endif
                                </div>

                                 <div class="form-group">
                                    <label for="recipient-name" class="control-label">First Name</label>
                                    <input type="text" name="driverFirstName" class="form-control" id="editFirstName">
                                </div>

                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Last Name</label>
                                    <input type="text" name="driverLastName" class="form-control" id="editLastName">
                                </div>
                                
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Email</label>
                                    <input type="email" name="driverEmail" class="form-control" id="editEmail" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Phone No</label>
                                    <input type="tel" name="driverPhone" class="form-control" id="editPhone">
                                </div>

                                 <div class="form-group">
                                    <label for="imageEdit" class="btn btn-info ">Edit Driver Image</label>
                                    <input type='file' style="visibility:hidden;" name="imageToUpload" id="imageEdit" /><br>
                                    <img height="120" width="120" id="driverImageEdit" src="#" alt="your image" />
                                </div>
                                
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-info submit-button">Edit Record</button>
                                    <button type="button" class="btn btn-danger cancel-button" data-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                        
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2018 - Imperial </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
</body>
@include('Admin.Assets.foot') 

<script>
$(function () {
  var driverUploadsPath = "{{env('DRIVER_UPLOADS')}}"
});   
</script>

<script src="{{URL::asset('handlers/simple-send.js')}}"></script>
<script src="{{URL::asset('handlers/drivers.js')}}"></script>
</html>