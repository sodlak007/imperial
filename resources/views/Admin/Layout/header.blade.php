
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                            <!-- Light Logo icon -->
                            <img src="{{URL::asset('images/logo.png')}}" alt="homepage" srcset="{{URL::asset('images/logo@2x.png 2x')}}" class="light-logo img-responsive" />
                         </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                      <li class="nav-item"> 
                        <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)">
                            <i class="mdi mdi-menu"></i>
                        </a> 
                    </li>
                       
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                     
                         
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{URL::asset('images/users/blank-profile-picture-973460_640-300x300.png')}}" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-text">
                                                <!-- <h4>Steave Jobs</h4> -->
                                                <p class="text-muted">{{\Auth::user()->email}}</p>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- <li role="separator" class="divider"></li>
                                    <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li> -->
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{{url('/api/logout')}}"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap">PERSONAL</li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="{{url('/admin/dashboard')}}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>
                           
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Bookings</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{url('/admin/bookings/all')}}?page=1">All Bookings</a></li>
                                <li><a href="{{url('/admin/bookings/pending')}}?page=1">Pending Bookings</a></li>
                                <li><a href="{{url('/admin/bookings/complete')}}?page=1">Confirmed Bookings</a></li>
                                <li><a href="{{url('/admin/bookings/cancelled')}}?page=1">Cancelled Bookings</a></li>
                                <li><a href="{{url('/admin/bookings/rejected')}}?page=1">Rejected Bookings</a></li>
                                <li><a href="{{url('/admin/bookings/completed')}}?page=1">Completed Rides</a></li>
                                <li><a href="app-calendar.html">Calendar</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-credit-card"></i><span class="hide-menu">Payments</span></a>
                             <ul aria-expanded="false" class="collapse">
                                <li><a href="{{url('/admin/payments/online')}}?page=1">Online</a></li>
                                <li><a href="{{url('/admin/payments/bank')}}?page=1">Bank</a></li>
                                <li><a href="{{url('/admin/refunds')}}?page=1">Refunds</a></li>
                         </ul>
                        </li>
                      
                      <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-account"></i><span class="hide-menu">User Management</span></a>
                            <ul aria-expanded="false" class="collapse">
                                @if(\Auth::user()->role == 'SUPERADMIN')
                                     <li><a href="{{url('/admin/user-management')}}?page=1">Admin User Access</a></li>
                                @endif
                                <li><a href="{{url('/admin/customers')}}?page=1">Customers</a></li>
                                <li><a href="{{url('admin/drivers')}}/all?page=1">Drivers</a></li>
                            </ul>
                            
                        </li>
                       

                        
                        <li class="two-column"> <a class="has-arrow waves-effect waves-dark" href="{{url('/admin/reports')}}?page=1" aria-expanded="false"><i class="mdi mdi-chart-bar"></i><span class="hide-menu">Reports</span></a>
                        </li>
    
                        <li> <a class="has-arrow waves-effect waves-dark" href="{{url('/admin/settings')}}" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Platform Settings</span></a></li>
                        
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>