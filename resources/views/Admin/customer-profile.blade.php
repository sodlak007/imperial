@include('Admin.Assets.head') 


<body class="fix-header fix-sidebar card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
         @include('Admin.Layout.header') 
       <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Customers</h3>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
               
                
                
                  <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            
                            <div class="card-body">
                                <small class="text-muted">Full Name</small>
                                <h6>{{$user->name}}</h6> 
                                <small class="text-muted">Email address </small>
                                <h6>{{$user->email}}</h6> 
                                <small class="text-muted p-t-10 db">Phone</small>
                                <h6>{{$user->phone}}</h6> 
                                <small class="text-muted p-t-10 db">Address</small>
                                <h6>{{$user->address}}</h6>
                                
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Ride History</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile" role="tab">Survey History</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Settings</a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="home" role="tabpanel">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                
                                                <th>Date</th>
                                                <th>Address</th>
                                                <th>Time</th>
                                                <th>Driver</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($bookings as $booking)
                                            <tr>
                                                <td>{{$booking->date}}</td>
                                                <td>{{$booking->pickUpAddress}}</td>
                                                <td> {{date("F j, Y", strtotime($booking->time))}}</td>
                                                <td>To complete</td>
                                                @if($booking->approvalStatus == 'COMPLETED')
                                                <td><span class="label label-success">Completed</span> </td>
                                                @endif
                                                 @if($booking->approvalStatus == 'CANCELLED')
                                                <td><span class="label label-danger">Cancelled</span> </td>
                                                @endif
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                    </div>
                                </div>
                                <!--second tab-->
                               <div class="tab-pane" id="profile" role="tabpanel">
                                    <div class="card-body">
                                
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Driver</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($surveys as $survey)
                                                    @if($survey['survey'] != null)
                                                <tr>
                                                    <td>{{date("F j, Y", strtotime($survey['survey']->created_at))}}</td>
                                                    <td>{{$survey['driver']->firstname}} {{$survey['driver']->lastname}}</td>
                                                    <td><button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#view-survey">View Response</button></td>
                                                </tr>

                                                <div id="view-survey" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel">Survey Feedback</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="survey-response">
                                                                <p>Rate your Imperial ride? <span>{{$survey['survey']->question_1}}</span></p>
                                                                <p>Tell us what we did right. <span>{{$survey['survey']->question_2}}</span></p>
                                                                <p>How can we serve you better? <span>{{$survey['survey']->question_3}}</span></p>
                                                                <p>Leave a comment to help us improve your ride experience <span>{{$survey['survey']->comment}}</span></p>
                                                            </div>
                                                        </div>
                                                       
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                @endif
                                                @endforeach
                                            </tbody>
                                        </table>

                                        {{-- <nav aria-label="Page navigation example">
                                          <ul class="pagination">
                                            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                          </ul>
                                        </nav> --}}
                                    </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="settings" role="tabpanel">
                                    <div class="card-body">
                                        <form class="form-horizontal form-material" method="POST" action="{{route('customer.update')}}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="id" value="{{$user->id}}">

                                            <div class="form-group">
                                                <label class="col-md-12">First Name</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="{{@explode(' ', $user->name)[0]}}" class="form-control form-control-line" disabled>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-12">Last Name</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="{{@explode(' ', $user->name)[1]}}" class="form-control form-control-line" disabled>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12">Email</label>
                                                <div class="col-md-12">
                                                    <input type="email" placeholder="{{$user->email}}" class="form-control form-control-line" name="email" id="example-email">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-12">Phone No</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="{{$user->phone}}" class="form-control form-control-line" name="phone">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button type="submit" class="btn btn-info">Update Profile</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>


              
            </div>

             

            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2018 - Imperial </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
   @include('Admin.Assets.foot') 
</body>

</html>