@include('Admin.Assets.head') 
<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register" style="background-image:url('{{URL::asset('images/background/uberlux-london-1050x656.jpg')}}');">
            <div class="login-box card">
                <div class="card-body">
                    {!! Form::open(array('url' => url("/api/admin/login"),'class'=>'form-horizontal  adminLogin','id'=>'loginform')) !!}
                        <h3 class="box-title m-b-20">Sign In</h3>
                        <div id="responses">
                             @if (Session::has('message'))
                               <div class="alert alert-success"> {{ Session::get('message') }} </div>                           
                             @endif
                        </div>
                        
                        <div class="form-group ">
                            <label for="newPassword">Email Address</label>
                            <input class="form-control" type="email" name="email"  placeholder="Email Address">
                        </div>
                        
                         <div class="form-group">
                            <label for="newPassword">Password</label>
                            <input class="form-control" type="password" name="password"  placeholder="Password">
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 font-14">
                                
                                <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right">Forgot password?</a> </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-md btn-block text-uppercase waves-effect waves-light submit-button" type="submit" disabled>Log In</button>
                            </div>
                        </div>
                        
                    </form>
                    {!! Form::open(array('url' => url("/api/recover"),'class'=>'form-horizontal  adminRecover','id'=>'recoverform')) !!}
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <h3>Recover Password</h3>
                                <p class="small">Enter your email address and a password reset link would be sent to your inbox.</p>
                                <div id="responses2">
                                </div>
                         </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                 <label for="newPassword">Email Address</label>
                                <input class="form-control" name="email" type="text" placeholder="Email"> 
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-md btn-block text-uppercase waves-effect waves-light recover-button" type="submit" disabled>Send Reset Link</button>
                            </div>
                        </div>
                        <div class="form-group m-b-0">
                            <div class="col-sm-12 text-center">
                                <div><a href="{{url('/admin/login')}}" class="text-info m-l-5"><b>Return to Login</b></a></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</body>
@include('Admin.Assets.foot') 
<script src="{{URL::asset('handlers/simple-send.js')}}"></script>
<script src="{{URL::asset('handlers/auth.js')}}"></script>
