@include('Admin.Assets.head') 
<body class="fix-header fix-sidebar card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
         @include('Admin.Layout.header') 
      
         <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Platform Settings</h3>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                
                
                <div class="row">
                    <div class="col-12">

                       <div class="card">
                            <div class="card-header">
                                Price
                            </div>
                            <div class="card-body">
                                @if($flash = session('pricing'))
                                <div class="alert alert-success">{{$flash}}<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button></div>
                                @endif
                                <form action="{{url('/admin/settings/pricing')}}" method="POST">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Pricing</label>
                                        <input  class="form-control" name="price" onkeypress="return isNumberKey(event)" value="{{env('PRICE')}}" required>
                                    </div>
                                   
                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Update</button>
                                </form>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                Cars in Fleet
                            </div>
                            <div class="card-body">
                                @if($flash = session('car'))
                                <div class="alert alert-success">{{$flash}}<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button></div>
                                @endif
                                <form action="{{url('/admin/settings/car-count')}}" method="POST">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Number of cars</label>
                                        <input type="text" class="form-control" name="car" value="{{$car->quantityInStock}}" required>
                                    </div>
                                   
                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Update</button>
                                </form>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                               Super Admin Password
                            </div>
                            <div class="card-body">
                                @if($flash = session('password'))
                                <div class="alert alert-success">{{$flash}}<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button> </div>
                                @endif

                                 @if($flash = session('error'))
                                <div class="alert alert-danger">{{$flash}}<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button> </div>
                                @endif

                                <form action="{{url('/admin/settings/password')}}" method="POST">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label>New Password</label>
                                        <input type="password" class="form-control" name="password" required>
                                    </div>

                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <input type="password" class="form-control" name="password_confirmation" required="">
                                    </div>
                                   
                                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
              
            </div>

            <footer class="footer"> © 2018 - Imperial </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
   @include('Admin.Assets.foot') 
   <script>
   function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode < 31 || (charCode >= 48 && charCode <= 57 ) || (charCode >= 96 && charCode <= 105 ))
                return true;
            return false;
        }
</script>
</body>
</html>