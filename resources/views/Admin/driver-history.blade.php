@include('Admin.Assets.head') 

<body class="fix-header fix-sidebar card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        @include('Admin.Layout.header') 
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Ride History</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/admin/drivers/all?page=1')}}">Drivers</a></li>
                        <li class="breadcrumb-item">Ride History</li>
                    </ol>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                
                @if($total == 0)
                  <div class="card">
                    <div class="card-body collapse show">
                       
                        <p class="card-text all-clear-text">All Clear <span>No ride history at the moment</span></p>
                    </div>
                </div>
                @else
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">    
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Date</th>
                                                <th>Customer</th>
                                                <th>Phone No</th>
                                                <th>Pickup Time</th>
                                                <th>Drop-off Time</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($driverHistory as $history)
                                            <tr>
                                                <td>{{$history->id}}</td>
                                                <td>{{date("F j, Y", strtotime($history->date))}}</td>
                                                <td>{{$history->name}}</td>
                                                <td>{{$history->phone}}</td>
                                                <td>{{date("g:i a", strtotime($history->time))}} </td>
                                                @if($history->dropOffTime == null)
                                                    <td>N/A</td>
                                                @else
                                                    <td>{{date("g:i a", strtotime($history->dropOffTime))}} </td>
                                                @endif
                                                
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                 @if($total > 20)
                                    <nav aria-label="Page navigation example">
                                      <ul class="pagination">
                                        @if($_GET['page'] != 1)
                                          <li class="page-item"><a class="page-link" href="{{url('/admin/rides/history/$driverId?page=')}}{{$_GET['page'] - 1}}">Previous</a></li>
                                        @else
                                          <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                        @endif

                                       @if((($_GET['page'] * 20) < $total) && ($total != 0))               
                                           <li class="page-item"><a class="page-link" href="{{url('/admin/rides/history/$driverId?page=')}}{{$_GET['page'] + 1}}">Next</a></li>
                                        @else
                                          <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                        @endif     
                                      </ul>
                                    </nav>
                                    @endif
                                </div>
                            </div>
                        </div>

                      
                    </div>
                </div>
               @endif
            </div>

            

          
            <footer class="footer"> © 2018 - Imperial </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
</body>
@include('Admin.Assets.foot') 
</html>