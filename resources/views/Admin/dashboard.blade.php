@include('Admin.Assets.head') 

<body class="fix-header fix-sidebar card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        @include('Admin.Layout.header') 
        
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Dashboard</h3>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-car text-info"></i></h2>
                                    <h3 class="">{{$availableCarsForBooking}}</h3>
                                    <h6 class="card-subtitle">Available Cars</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-calendar-check text-success"></i></h2>
                                    <h3 class="">{{$bookingsToday}}</h3>
                                    <h6 class="card-subtitle">New Bookings</h6></div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-calendar-question text-purple"></i></h2>
                                    <h3 class="">{{$pendingBookings}}</h3>
                                    <h6 class="card-subtitle">Pending Bookings</h6></div>
                               
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-wallet text-warning"></i></h2>
                                    <h3 class="">&#x20a6;{{$totalEarningsToday}}</h3>
                                    <h6 class="card-subtitle">Today's Earnings</h6></div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <button type="button" class="btn waves-effect waves-light btn-info" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#new-book">New Booking</button>
                        <br/><br/>
                    </div>
                </div>

                @if(count($recentPendingBookings) == 0)
                  <div class="card">
                    <div class="card-body collapse show">
                       
                        <p class="card-text all-clear-text">All Clear <span>No bookings at the moment</span></p>
                    </div>
                </div>
                @else
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Recent Bookings </h4>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                               
                                                <th>Full Name</th>
                                                <th>Email</th>
                                                <th>Phone No</th>
                                                <th>Pickup Address</th>
                                                <th>Date & Time</th>
                                                <th>Time</th>
                                                <th>Action</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($recentPendingBookings as $booking)
                                            <tr>
                                               
                                                <td>{{$booking->name}}</td>
                                                <td>{{$booking->email}}</td>
                                                <td>{{$booking->phone}}</td>
                                                <td>{{$booking->pickUpAddress}}</td>
                                                <td>{{date("F j, Y", strtotime($booking->date))}}</td>
                                                <td>{{date("g:i a", strtotime($booking->time))}}</td>
                                                <td>
                                                    <button type="button" onclick="setAcceptUrl('{{url('/admin/accept/booking/')}}/{{$booking->id}}')" class="btn btn-xs btn-success accept-book" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#accept-book"><i class="fa fa-check"></i></button>
                                                    <button type="button" onclick="setRejectUrl('{{url('/admin/reject/booking/')}}/{{$booking->id}}')" class="btn waves-effect waves-light btn-xs btn-danger" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#reject-book" ><i class="fa fa-times"></i></button>
                                                </td>
                                            </tr>
                                            @endforeach
                                           
                                          
                                        </tbody>
                                    </table>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @endif


              
            </div>

              <div id="accept-book" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Confirm Booking</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to confirm this booking?</p>
                        </div>
                        <div class="modal-footer">
                            <button  type="button" class="btn btn-danger cancel-button" data-dismiss="modal">Cancel</button>
                            <button onclick="doAccept();" type="button" class="btn btn-success accept-button">Yes, Please</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                 <!-- /.modal-dialog -->
              </div>
          
              <div id="reject-book" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Reject Booking</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to reject this booking?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger cancel-button" data-dismiss="modal">Cancel</button>
                            <button onclick="doReject();" type="button" class="btn btn-success reject-button">Yes, Please</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

            

            <div id="new-book" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">New Booking</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <br><span id="err" style="display:none; padding-left: 17px; color:red;"></span>
                        <div class="modal-body">
                           {!! Form::open(array('url' => url("/bookings/create"),'class'=>'bookingCreate','id'=>'createForm')) !!}

                                <div id="responses">
                                    @if (Session::has('message'))
                                    <div class="alert alert-success"> {{ Session::get('message') }} </div>                           
                                    @endif
                                </div>
                                
                               <div class="form-group">
                                    <label for="recipient-name" class="control-label">Full Name</label>
                                    <input type="text" name="fullname" class="form-control" id="recipient-name1">
                                </div>
                                
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Email</label>
                                    <input type="email" name="email" class="form-control" id="recipient-name1">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Phone No</label>
                                    <input type="tel" name="phone" class="form-control" id="recipient-name1">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Pickup Address</label>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon3"><i class="fa fa-map-marker"></i></span>
                                        <input type="text" name="pickUpAddress" class="form-control" id="basic-url" aria-describedby="basic-addon3">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Pickup Date</label>
                                    <input type="text" name="pickUpDate" data-date-format='yyyy-mm-dd' class="form-control" id="datepicker-autoclose" placeholder="yyyy-mm-dd">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Pickup Time</label>
                                    <input type="text"  name="pickUpTime" class="form-control" id="single-input">
                                </div>
                                
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-info submit-button">Create Booking</button>
                                    <button type="button"  class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                        
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2018 - Imperial </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>

</body>
@include('Admin.Assets.foot') 
<script src="{{URL::asset('handlers/simple-send.js')}}"></script>
<script src="{{URL::asset('handlers/bookings.js')}}"></script>
   
</html>