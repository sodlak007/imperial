@include('Admin.Assets.head') 
<body class="fix-header fix-sidebar card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        @include('Admin.Layout.header') 
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Refunds</h3>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
               
                 @if($total == 0)
                  <div class="card">
                    <div class="card-body collapse show">
                       
                        <p class="card-text all-clear-text">All Clear <span>No Refunds at the moment</span></p>
                    </div>
                </div>
                @else
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                
                               <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Ride ID</th>
                                                <th>Full Name</th>
                                                <th>Bank</th>
                                                <th>Account No</th>
                                                <th>Amount</th>
                                                <th>Status</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($refunds as $refund)
                                            <tr>
                                                <td>{{$refund->rideId}}</td>
                                                <td>{{$refund->accountName}}</td>
                                                <td>{{$refund->bank}}</td>
                                                <td>{{$refund->accountNo}}</td>
                                                @if($refund->hasFine == 1)
                                                    <td>N{{ env('PRICE') - ($refund->finePercentage / 100) * env('PRICE')}}</td>
                                                @else
                                                    <td>{{env('PRICE')}}</td>
                                                @endif

                                               @if($refund->status == 'PENDING')
                                                    <td><button data-toggle="modal" data-backdrop="static" data-target="#confirm-refund" onclick="setPaidUrl('{{url('/admin/confirm/refund/')}}/{{$refund->id}}')" type="button" class="btn btn-xs btn-info">Accept</button></td>
                                               @else
                                                    <td><span class="label label-success">Paid</span></td>
                                               @endif
                                            </tr>
                                            @endforeach
                                          
                                          
                                        </tbody>
                                    </table>

                                    @if($total > 20)
                                    <nav aria-label="Page navigation example">
                                      <ul class="pagination">
                                        @if($_GET['page'] != 1)
                                          <li class="page-item"><a class="page-link" href="{{url('/admin/refunds?page=')}}{{$_GET['page'] - 1}}">Previous</a></li>
                                        @else
                                          <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                        @endif

                                       @if((($_GET['page'] * 20) < $total) && ($total != 0))               
                                           <li class="page-item"><a class="page-link" href="{{url('/admin/refunds?page=')}}{{$_GET['page'] + 1}}">Next</a></li>
                                        @else
                                          <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                        @endif     
                                      </ul>
                                    </nav>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
              
            </div>

            <div id="confirm-refund" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Confirm Refund</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <p>Click "Yes" to confirm refund and notify Customer.</p><br>
                             <span id="message" style="color:red"></span>
                        </div>
                        
                        <div class="modal-footer">
                            <button  type="button" class="btn btn-danger cancel-button" data-dismiss="modal">Cancel</button>
                            <button onclick="markAsPaid();" type="button" class="btn btn-success accept-button">Yes, Please</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                 <!-- /.modal-dialog -->
              </div>

          
             
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2018 - Imperial </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    
</body>
@include('Admin.Assets.foot') 
<script src="{{URL::asset('handlers/simple-send.js')}}"></script>
<script src="{{URL::asset('handlers/payments.js')}}"></script>
</html>