@include('Admin.Assets.head') 


<body class="fix-header fix-sidebar card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
         @include('Admin.Layout.header') 
       <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Customers</h3>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
               
                
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Last Name</th>
                                                <th>First Name</th>
                                                <th>Email</th>
                                                <th>Phone No</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = ( ($_GET['page'] - 1) * 20) +  1; ?>
                                          @foreach($data as $datum)
                                            <tr>
                                                <td><a href="javascript:void(0)">{{$i++}}</a></td>
                                                <td>{{@explode(' ',$datum->name,2)[0]}}</td>
                                                <td>{{@explode(' ',$datum->name,2)[1]}}</td>
                                                <td>{{$datum->email}}</td>
                                                <td>{{$datum->phone}}</td>
                                                <td><a href="/admin/customers/get/{{$datum->id}}" class="btn btn-xs btn-info">View Profile</a></td>
                                           </tr>
                                            @endforeach
                                          
                                        </tbody>
                                    </table>

                                   @if($total > 20)
                                    <nav aria-label="Page navigation example">
                                      <ul class="pagination">
                                        @if($_GET['page'] != 1)
                                          <li class="page-item"><a class="page-link" href="{{url('/admin/customers?page=')}}{{$_GET['page'] - 1}}">Previous</a></li>
                                        @else
                                          <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                        @endif

                                       @if( (($_GET['page'] * 20) < $total) && ($total != 0))               
                                        <li class="page-item"><a class="page-link" href="{{url('/admin/customers?page=')}}{{$_GET['page'] + 1}}">Next</a></li>
                                        @else
                                          <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                        @endif     
                                      </ul>
                                    </nav>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              
            </div>

            
            <div id="confirm-book" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Confirm Booking</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <h5 class="m-t-30 m-b-10">Payment Channel</h5>
                                    <select class="selectpicker" data-style="form-control btn-secondary">
                                        <option>Online Payment</option>
                                        <option>Bank Transfer</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Transaction Ref</label>
                                    <input type="text" class="form-control" id="recipient-name1">
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-info">Confirm</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2018 - Imperial </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
   @include('Admin.Assets.foot') 
</body>

</html>