@include('Admin.Assets.head') 
<body class="fix-header fix-sidebar card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
         @include('Admin.Layout.header') 
      
         <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Reports</h3>
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                
                
               
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Select Filter</label>
                                            <select id="status" class="form-control custom-select">
                                                <option value="">Select an option</option>
                                                <option value="PENDING">Pending Rides</option>
                                                <option value="CONFIRMED">Confirmed Rides</option>
                                                <option value="CANCELLED">Cancelled Rides</option>
                                                <option value="COMPLETED">Completed Rides</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Keyword Search</label>
                                            <input type="text" id="keyword" value="{{@$_GET['keyword']}}" class="form-control" placeholder="payee name, email or reference">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Date Range</label>
                                            <div class="input-daterange input-group" id="date-range">
                                                <input type="text" id="datepicker-from" value="{{@$_GET['start']}}" data-date-format='yyyy-mm-dd' class="form-control" name="start" placeholder="From" />
                                                <span class="input-group-addon bg-info b-0 text-white">to</span>
                                                <input type="text" id="datepicker-to" value="{{@$_GET['end']}}" data-date-format='yyyy-mm-dd' class="form-control" name="end" placeholder="To" />
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span--> 
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <button type="submit" onclick="filter()" class="btn btn-info btn-block" style="height: 45px;"> <i class="fa fa-check"></i> Search</button>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                            </div>
                        </div>



                        <div class="card">
                            <div class="card-body">
                                
                               
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Full Name</th>
                                                <th>Email</th>
                                                <th>Phone No</th>
                                                <th>Amount</th>
                                                <th>Date</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            @foreach($transactions as $transaction)
                                            <tr>
                                                <td>{{$transaction->name}}</td>
                                                <td>{{$transaction->email}}</td>
                                                <td>{{$transaction->phone}}</td>
                                                <td>{{$transaction->amount}}</td>
                                                <td><span class="text-muted"><i class="fa fa-clock-o"></i> {{date("F j, Y", strtotime($transaction->created_at))}}</span> </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                    @if($total > 20)
                                    <nav aria-label="Page navigation example">
                                      <ul class="pagination">
                                        @if($_GET['page'] != 1)
                                          <li class="page-item"><a class="page-link" href="{{url('/admin/reports?page=')}}{{$_GET['page'] - 1}}{{$filter}}">Previous</a></li>
                                        @else
                                          <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                        @endif

                                       @if((($_GET['page'] * 20) < $total) && ($total != 0))               
                                           <li class="page-item"><a class="page-link" href="{{url('/admin/reports?page=')}}{{$_GET['page'] + 1}}{{$filter}}">Next</a></li>
                                        @else
                                          <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                        @endif     
                                      </ul>
                                    </nav>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
              
            </div>

            <footer class="footer"> © 2018 - Imperial </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
   @include('Admin.Assets.foot') 

   <!-- start - This is for export functionality only -->
    <!-- <script src="{{URL::asset('plugins/datatables/jquery.dataTables.min.js')}}"></script> -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
   <script>
    $(function() {
        var status = "{{@$_GET['bookingStatus']}}"
        $("#status").val(status);
    });

   jQuery('#datepicker-from').datepicker({
    autoclose: true,
    todayHighlight: true
    });

    jQuery('#datepicker-to').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    function filter(){
        var base = "{{$base}}";
        var keyword = $('#keyword').val();
        var bookingStatus = $('#status').val();
        var start = $('#datepicker-from').val();
        var end = $('#datepicker-to').val();

        var filter = "";

        if(keyword != "")
           filter += "&keyword="+keyword
        
        if(bookingStatus != "")
           filter += "&bookingStatus="+bookingStatus

        if(start != "" && end != "")
           filter += "&start="+start+"&end="+end;

        filter = "{{url('/')}}" + base + filter;

        window.location = filter
    }

    $(document).ready( function () {
        $('#myTable').DataTable();
    } );

    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
</script>
</body>
</html>