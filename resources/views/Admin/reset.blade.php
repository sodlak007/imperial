@include('Admin.Assets.head') 
<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register" style="background-image:url('{{URL::asset('images/background/uberlux-london-1050x656.jpg')}}');">
            <div class="login-box card">
                <div class="card-body">
                    {!! Form::open(array('url' => url("/api/reset"),'class'=>'form-horizontal adminReset','id'=>'resetform')) !!}
                    <input name="token" type="hidden" value="{{$_GET['token']}}">
                        <h3 class="box-title m-b-20">Create New Password</h3>
                            <div id="responses">
                                @if (Session::has('message'))
                                   <div class="alert alert-success"> {{ Session::get('message') }} </div>                           
                                @endif
                            </div>
                        
                        <div class="form-group">
                            <label for="newPassword">New Password</label>
                            <input class="form-control" type="password" name="password" id="password" placeholder="Password"> 
                        </div>
                        <div class="form-group">
                            <label for="newPassword">Confirm Password</label>
                            <input class="form-control" type="password" name="passwordConfirm" id="passwordconfirm"  placeholder="Confirm Password"> 
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 font-14">
                                <a style="display:none;" href="{{url('/admin/login')}}" id="to-recover" class="text-dark pull-right ">Go to Login</a> 
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-md btn-block text-uppercase waves-effect waves-light reset-button" type="submit" disabled>Create Password</button>
                            </div>
                        </div>
                        
                    </form>
                    
                </div>
            </div>
        </div>
    </section>
</body>
@include('Admin.Assets.foot') 
<script src="{{URL::asset('handlers/simple-send.js')}}"></script>
<script src="{{URL::asset('handlers/auth.js')}}"></script>
