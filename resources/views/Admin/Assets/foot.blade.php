 <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{URL::asset('plugins/jquery/jquery.min.js')}}"></script>
    <script src="{{URL::asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{URL::asset('js/jquery.timepicker.min.js')}}"></script>
   
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{URL::asset('plugins/bootstrap/js/popper.min.js')}}"></script>
    <script src="{{URL::asset('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src=" {{URL::asset('js/jquery.slimscroll.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{URL::asset('js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{URL::asset('js/sidebarmenu.js')}}"></script>
    <!--stickey kit -->
    <script src="{{URL::asset('plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
    <script src="{{URL::asset('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{URL::asset('js/custom.min.js')}}"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="{{URL::asset('plugins/styleswitcher/jQuery.style.switcher.js')}}"></script>
    <script src="{{URL::asset('js/app.js')}}"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>