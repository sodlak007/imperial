@include('Website.Assets.head') 
<body>


<div id="fullpage">
	<div id="header">
		<img src="{{URL::asset('images/logo.png')}}" srcset="{{URL::asset('images/logo@2x.png')}} 2x">
	</div>
	
	<div class="section " id="section0">
		<div class="jumbotext">
			<span>RENT ONE OF OUR  CARS AND GET THERE IN STYLE</span>
			<h1>Today belongs to you</h1>
			<button href="#" class="full-modal">Book a car</button>
		</div>

		<div class="links">
			<a href="#howItWorks">How it works</a>
			<a href="#contact">Contact Us</a>
		</div>
		
	</div>

	<div class="section" id="section1">
		<div class="show-desktop">
			<div class="">
				<div class="section-content">
					<h2>How the imperial works</h2>
					<p class="book-easy">Booking a ride is so easy</p>

					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="card">
								<img src="{{URL::asset('Website/img/step-1.svg')}}">
								<p>Book a car
									<span>Make a reservation with our online booking form and you would be contacted if your selected date is available.</span>
								</p>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="card">
								<img src="{{URL::asset('Website/img/step-2.svg')}}">
								<p>Pay Online
									<span>Make payment online or via bank transfer and your booking process is completed</span>
								</p>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="card">
								<img src="{{URL::asset('Website/img/step-3.svg')}}">
								<p>Ride in Style
									<span>Your driver would arrive on your reserved date to pick you up. You can enjoy the full luxury experience imperial taxi has to offer.</span>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="show-mobile">
			<div class="fixed-hiw-content">
				<p>How it works <span>Booking a ride is so easy</span></p>
			</div>
			
			<div class="slide" id="slide1" data-anchor="slide1">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-12">
							
							<div class="card">
								<img src="{{URL::asset('Website/img/step-1.svg')}}">
								<p>Book a car
									<span>Make a reservation with our online booking form and you would be contacted if your selected date is available.</span>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slide" id="slide2" data-anchor="slide2">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-12">
							
							<div class="card">
								<img src="{{URL::asset('Website/img/step-2.svg')}}">
								<p>Pay Online
									<span>Make payment online or via bank transfer and your booking process is completed</span>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slide" id="slide1" data-anchor="slide3">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-12">
							
							<div class="card">
								<img src="{{URL::asset('Website/img/step-3.svg')}}">
								<p>Ride in Style
									<span>Your driver would arrive on your reserved date to pick you up. You can enjoy the full luxury experience imperial taxi has to offer.</span>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	

	<div class="section" id="section3">
		<div class="">
			<div class="contact-ctn clearfix">
				<div class="left-side ctn-box">
					<h2>Get in touch</h2>
					<p>We are always available to resolve any issues you may have at any point in time. You can reach us via email or by calling us on our customer support line.</p>
				</div>
				<div class="right-side ctn-box">
					<div class="clearfix telphone">
						<img src="{{URL::asset('Website/img/ctn-tel.svg')}}">
						<p>You can reach us on <span><a href="tel:+2349022445943">+ 234 902 244 5943</a></span><span><a href="tel:+2348088353586">+ 234 808 835 3586</a></span></p>
					</div>

					<div class="clearfix">
						<img src="{{URL::asset('Website/img/ctn-email.svg')}}">
						<p>Send us an email at<span><a href="mailto:info@theimperial.com">info@theimperial.com</a></span></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="success-modal" class="full-bg-modal animated">
	<a href="#" class="close-mod"><img src="{{URL::asset('Website/img/close.svg')}}"></a>

	<div class="form-div">
			<center>
				<img src="{{URL::asset('Website/img/dark-form.png')}}" srcset="{{URL::asset('Website/img/dark-form@2x.png')}} 2x">
			</center>

			<div class="success-message">
				<img src="{{URL::asset('Website/img/checkmark.svg')}}">

				<h2>Booking details sent</h2>
				<p>Your booking information has been submitted. You will be notified via email once your booking has been confirmed</p>
				<button onclick="location.href = '{{url('/')}}'" class="go-home">Return home</button>
			</div>
		
	</div>
</div>

<div id="request-modal" class="full-bg-modal animated">
	<a href="#" class="close-mod"><img src="{{URL::asset('Website/img/close.svg')}}"></a>

	<div class="form-div">
			<center>
				<img src="{{URL::asset('Website/img/dark-form.png')}}" srcset="{{URL::asset('Website/img/dark-form@2x.png')}} 2x">
			</center>

            <h1>Book A Ride</h1>
            <p style="text-align: center;">Our exquisite rides are Chrysler Cadillac 300</p>
            
           <div id="responses"></div>
		

		   {!! Form::open(array('url' => url("/bookings/create"),'class'=>'form-horizontal bookingCreate','id'=>'createForm')) !!}
                   
			<div class="row">
				
					<div class="col-md-6 col-sm-12">
						<div id="firstname" class="form-wrap">
						<label>First Name</label>
						<input type="text" name="firstname">
					  </div>
				   </div> 
				
					<div class="col-md-6 col-sm-12">
						<div id="lastname" class="form-wrap">
							<label>Last Name</label>
							<input type="text" name="lastname">
					  </div>
				</div>
			</div>

			<div class="row">
				
					<div class="col-md-6 col-sm-12">
						<div id="email" class="form-wrap">
						<label>Email Address</label>
						<input type="email" name="email">
					</div>
				</div>
				
					<div class="col-md-6 col-sm-12">
						<div id="phone" class="form-wrap">
						<label>Phone Number</label>
						<input type="tel" name="phone">
					</div>
				</div>
			</div>

			<div class="row">
				
				<div  class="col-md-6 col-sm-12">
					<div id="pickUpDate" class="form-wrap">
					<label>Pickup Date</label>
					<input data-date-format='yyyy-mm-dd' type="text" class="mydatepicker" name="pickUpDate">
				</div>
				</div>
				
				<div  class="col-md-6 col-sm-12">
					<div id="pickUpTime" class="form-wrap">
                    <label>Pickup Time</label>
                    <select name="pickUpTime" style=" width: 100%;height: 45px;-webkit-appearance: none;padding: 10px;border-radius: 3px;">
                        @for($i = 10; $i < 24; $i++)
                            <option value="{{$i}}:00:00">{{$i}}:00:00</option>
                        @endfor
                        @for($i = 0; $i < 10; $i++)
                            <option value="0{{$i}}:00:00">0{{$i}}:00:00</option>
                        @endfor
                    </select>
				</div>
				</div>
			</div>

			<div class="row">
				
				<div  class="col-sm-12">
					<div id="pickUpAddress" class="form-wrap">
					<label>Pickup Address</label>
					<textarea name="pickUpAddress"></textarea>
				</div>
				</div>
				
			</div>

			<div class="row">
				
				<div class="col-sm-12">
					<button type="submit" class="submit-button">Book Ride</button>
				</div>
				
			</div>
		</form>
		
	</div>
</div>
@include('Website.Assets.foot') 
<script src="{{URL::asset('handlers/simple-send.js')}}"></script>
<script src="{{URL::asset('handlers/front.js')}}"></script>

