@include('Website.Assets.head') 
<body>

	<div class="form-div">
		<center>
			<img src="{{URL::asset('Website/img/dark-form.png')}}" srcset="{{URL::asset('Website/img/dark-form@2x.png')}} 2x">
		</center>

		<div class="success-message">
			<img src="{{URL::asset('Website/img/checkmark.svg')}}">

			<h2>Request Sent</h2>
			<p>Your request is being processed and you will be notified once it has been processed.</p>
			<button onclick="location.href = '{{url('/')}}'" class="go-home">Return home</button>
	</div>
	@include('Website.Assets.foot') 

</body>
</html>