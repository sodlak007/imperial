@include('Website.Assets.head') 
<body>

	<div class="form-div">
		<center>
			<img src="{{URL::asset('img/dark-form.png')}}" srcset="{{URL::asset('img/dark-form@2x.png 2x')}}">
		</center>

		<h1>Ride Experience Survey</h1>
			@if($flash = session('message'))
			<div class="alert alert-danger">This ride has been reserved for more than 24hours, a penalty of  </div>
			@endif
		<form method="POST" action="{{route('survey.submit')}}">
			{{ csrf_field() }}
			<input type="hidden" name="booking_id" value="{{$id['id']}}">
			<div class="row">
				<div class="col-sm-12">
					<div class="form-wrap">
						<label><b>Rate your Imperial ride?</b></label>
						<div class="form-group" style="margin-top: 10px;">
						    <div class="form-check">
						       <input class="form-check-input" type="radio" name="question_1" id="gridRadios1" value="One Star" checked>
					          <label class="form-check-label" for="gridRadios1">
					           <i class="fas fa-chess-queen"></i>
					          </label>
						    </div>
						    <div class="form-check">
						       <input class="form-check-input" type="radio" name="question_1" id="gridRadios1" value="Two Stars" checked>
					          <label class="form-check-label" for="gridRadios1">
					            <i class="fas fa-chess-queen"></i><i class="fas fa-chess-queen"></i>
					          </label>
						    </div>
						    <div class="form-check">
						       <input class="form-check-input" type="radio" name="question_1" id="gridRadios1" value="Three Stars" checked>
					          <label class="form-check-label" for="gridRadios1">
					           <i class="fas fa-chess-queen"></i><i class="fas fa-chess-queen"></i><i class="fas fa-chess-queen"></i>
					          </label>
						    </div>
						    <div class="form-check">
						       <input class="form-check-input" type="radio" name="question_1" id="gridRadios1" value="Four Stars" checked>
					          <label class="form-check-label" for="gridRadios1">
					            <i class="fas fa-chess-queen"></i><i class="fas fa-chess-queen"></i><i class="fas fa-chess-queen"></i><i class="fas fa-chess-queen"></i>
					          </label>
						    </div>
						    <div class="form-check">
						       <input class="form-check-input" type="radio" name="question_1" id="gridRadios1" value="Five Stars" checked>
					          <label class="form-check-label" for="gridRadios1">
					            <i class="fas fa-chess-queen"></i><i class="fas fa-chess-queen"></i><i class="fas fa-chess-queen"></i><i class="fas fa-chess-queen"></i><i class="fas fa-chess-queen"></i>
					          </label>
						    </div>
						</div>
					</div>
				</div>

				<div class="col-sm-12">
					<div class="form-wrap">
						<label><b>Tell us what we did right.</b></label>
						<div class="form-group" style="margin-top: 10px;">
						    <div class="form-check">
						      <input class="form-check-input" type="checkbox" id="gridCheck1" name="question_2[]" value="Clean Car">
						        <label class="form-check-label" for="gridCheck1">
						          Clean Car
						        </label>
						    </div>
						    <div class="form-check">
						      <input class="form-check-input" type="checkbox" id="gridCheck1" name="question_2[]" value="Pickup Time">
						        <label class="form-check-label" for="gridCheck1">
						          Pickup Time
						        </label>
						    </div>
						    <div class="form-check">
						      <input class="form-check-input" type="checkbox" id="gridCheck1" name="question_2[]" value="Route & Driving">
						        <label class="form-check-label" for="gridCheck1">
						          Route & Driving
						        </label>
						    </div>
						    <div class="form-check">
						      <input class="form-check-input" type="checkbox" id="gridCheck1" name="question_2[]" value="Courteous Driver">
						        <label class="form-check-label" for="gridCheck1">
						          Courteous Driver
						        </label>
						    </div>
						    <div class="form-check">
						      <input class="form-check-input" type="checkbox" id="gridCheck1" name="question_2[]" value="Service">
						        <label class="form-check-label" for="gridCheck1">
						          Service
						        </label>
						    </div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				
				<div class="col-sm-12">
					<div class="form-wrap">
						<label><b>How can we serve you better?</b></label>
						<div class="form-group" style="margin-top: 10px;">
						    <div class="form-check">
						      <input class="form-check-input" type="checkbox" id="gridCheck1" name="question_3[]" value="Car Quality">
						        <label class="form-check-label" for="gridCheck1">
						         Car Quality
						        </label>
						    </div>
						    <div class="form-check">
						      <input class="form-check-input" type="checkbox" id="gridCheck1" name="question_3[]" value="Pickup Time">
						        <label class="form-check-label" for="gridCheck1">
						          Pickup Time
						        </label>
						    </div>
						    <div class="form-check">
						      <input class="form-check-input" type="checkbox" id="gridCheck1" name="question_3[]" value="Routing & Driving">
						        <label class="form-check-label" for="gridCheck1">
						          Routing & Driving
						        </label>
						    </div>
						    <div class="form-check">
						      <input class="form-check-input" type="checkbox" id="gridCheck1" name="question_3[]" value="Ride Extras">
						        <label class="form-check-label" for="gridCheck1">
						          Ride Extras
						        </label>
						    </div>
						    <div class="form-check">
						      <input class="form-check-input" type="checkbox" id="gridCheck1" name="question_3[]" value="Service">
						        <label class="form-check-label" for="gridCheck1">
						          Service
						        </label>
						    </div>
						</div>
					</div>
				</div>
				
				<div class="col-sm-12">
					<div class="form-wrap">
						<label><b>Leave a comment to help us improve your ride experience</b></label>
						<textarea name="feedback"></textarea>
					</div>
				</div>
			</div>


			<div class="row">
				
				<div class="col-sm-12">
					<button type="submit">Submit</button>
				</div>
			</div>
		</form>
		
	</div>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
	<script src="js/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
	<script>
    jQuery(document).ready(function() {
        
        // For select 2
        $(".select2").select2();
	    });
	
	</script>

</body>
</html>