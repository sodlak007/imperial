<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>


<script type="text/javascript" src="{{URL::asset('Website/js/scrolloverflow.js')}}"></script>

<script type="text/javascript" src="{{URL::asset('Website/js/jquery.fullPage.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('Website/js/matchHeight.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('Website/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.timepicker.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('Website/js/jquery.animator-beta.min.js')}}"></script>
   
<script type="text/javascript">
	$(document).ready(function() {
		$('#fullpage').fullpage({
			anchors: ['home', 'howItWorks', 'contact'],
			sectionsColor: ['#000000', '#000000', '#000000'],
			navigation: true,
			slidesNavigation: true,
			navigationPosition: 'right'
			// navigationTooltips: ['First page', 'Second page', 'Third and last page']
		});

		$(function() {
			$('.card').matchHeight();
			$('.ctn-box').matchHeight();
		});

		 $('.full-modal').click(function(e){
		 	e.preventDefault();
		 	$('#request-modal').fadeToggle();
		 })

		 $('.close-mod').click(function(e){
		 	e.preventDefault();
		 	$('#request-modal').fadeOut();
    	 	$('#success-modal').fadeOut();
	     })
         
    
         $('.mydatepicker').datepicker();
         
     	 $('#timepicker').timepicker({ 'timeFormat': 'H:i:s' , 'step' : '60' })
	
	});
</script>


</body>
</html>