@include('Website.Assets.head') 
<body>

	<div class="form-div">
		<center>
			<img src="{{URL::asset('img/dark-form.png')}}" srcset="{{URL::asset('img/dark-form@2x.png 2x')}}">
		</center>
			<div class="success-message">
				<img src="{{URL::asset('img/checkmark.svg')}}">

				<h2>Thank you for your feedback.</h2>
				<p>We appreciate you taking the time to leave feedback. We will review your feedback and use it in our planning process going forward</p>
				<a href="/"><button class="go-home">Return home</button></a>
			</div>
	</div>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
	<script src="js/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
	<script>
    jQuery(document).ready(function() {
        
        // For select 2
        $(".select2").select2();
	    });
	
	</script>

</body>
</html>