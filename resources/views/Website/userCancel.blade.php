@include('Website.Assets.head') 
<body>

	<div class="form-div">
		<center>
			<img src="{{URL::asset('images/dark-form.png')}}" srcset="{{URL::asset('images/dark-form@2x.png')}} 2x">
		</center>

    	<h1>Cancel Ride</h1>

		@if($bookingMessage !== "")
			<div class="alert alert-danger">{{$bookingMessage}}</div>
		@else
		{!! Form::open(array('url' => url("/api/request-cancel/$rideID"),'class'=>'form-horizontal  userCancel','id'=>'userCancelForm')) !!}
			<div id="responses"></div>
			@if($fine_message)
			    <input type="hidden" name="hasFine" value="1">
				<div class="alert alert-danger">This ride has been reserved for more than 24hours, a 10% deduction applies. </div>
			@else
				 <input type="hidden" name="hasFine" value="0">
			@endif
						

				<div class="row">
					
					<div class="col-sm-12">
						<div id="reason" class="form-wrap">
							<label>Reason for Cancellation</label>
							<input type="text" name="reason">
						</div>
					</div>
					
					<div class="col-sm-12">
						<div class="form-wrap">
							<label>Name of Bank</label>
							<select class="select2 form-control custom-select" name="bank" style="width: 100%; height:36px;">
								
								<option>Select</option>
								<option >Access Bank</option>
								<option >Citibank</option>
								<option >Diamond Bank</option>
								<option >Ecobank Nigeria </option>
								<option >Fidelity Bank Nigeria</option>
								<option >First Bank of Nigeria</option>
								<option >First City Monument Bank</option>
								<option >Guaranty Trust Bank</option>
								<option >Heritage Bank plc </option>
								<option >Keystone Bank Limited</option>
								<option >Skye Bank</option>
								<option >Stanbic IBTC Bank Nigeria Limited</option>
								<option >Standard Chartered Bank</option>
								<option >Sterling Bank</option>
								<option >Union Bank of Nigeria</option>
								<option >United Bank for Africa</option>
								<option >Unity Bank plc</option>
								<option >Wema Bank</option>
								<option >Zenith Bank</option>
							
							</select>
						</div>
					</div>
				</div>

				<div class="row">
					
					<div  class="col-sm-12">
						<div id="accNumber" class="form-wrap">
							<label>Account Number</label>
							<input type="tel"  name="accNumber">
						</div>
					</div>
					
					<div  class="col-sm-12">
						<div id="accHolder" class="form-wrap">
							<label>Name of Account Holder</label>
							<input type="text" name="accHolder" >
						</div>
					</div>
				</div>


				<div class="row">
					
					<div class="col-sm-12">
						<button type="submit" class="submit-button">Cancel Ride</button>
					</div>
				</div>
			</form>
		@endif
	</div>
    @include('Website.Assets.foot') 
	<script src="{{URL::asset('handlers/simple-send.js')}}"></script>
	<script src="{{URL::asset('handlers/front.js')}}"></script>
	<script>
    jQuery(document).ready(function() {
        
        // For select 2
        $(".select2").select2();
	    });
	
	</script>


</body>
</html>