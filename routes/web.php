<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Operations\Front\FrontController@home');

Route::get('login', function () {
    return redirect('admin/login');
})->name('login');

Route::group(['middleware' => 'guest'], function(){
    Route::get('admin', function () {
        return redirect('admin/login');
    });
});


Route::post('/bookings/create', 'Operations\Bookings\Bookings@create');
Route::get('/ride/cancel/{rideID}', 'Operations\Bookings\Bookings@cancelFromUser');
Route::get('/ride/cancel-success', 'Operations\Bookings\Bookings@showSuccess');

Route::get('/ride/survey/{rideId}','Operations\Survey\SurveyController@index');
Route::post('survey/submit','Operations\Survey\SurveyController@store')->name('survey.submit');


Route::group(['prefix' => 'api'], function () {

    Route::post('/login', 'Authentication\LoginController@login');
    Route::get('/logout','Authentication\LoginController@logout');
       
    Route::post('/recover', 'Authentication\LoginController@recover');
    Route::post('/reset', 'Authentication\LoginController@doReset');
    Route::post('/request-cancel/{id}', 'Operations\Bookings\ConfirmedBookings@logCancelRequest');

    Route::group(['prefix' => 'admin'], function () {
    
        Route::post('/login', 'Authentication\LoginController@login');
        
    });
});

Route::group(['prefix' => 'cron'], function () {

    Route::get('/test', 'Operations\CronController@doDriver24HourNotify');
       
});


Route::group( ['prefix' => 'admin'], function () {
    Route::get('/login', 'Authentication\LoginController@showAdminLogin');
    Route::get('/reset', 'Authentication\LoginController@showReset');
    Route::get('/reset-notification', 'Authentication\LoginController@showResetNotify');
    Route::get('/success-notification', 'Authentication\LoginController@showSuccessNotify');
    Route::get('/forgot', 'FrontController@forgot');
   
    Route::group(['middleware' => ['web','before' => 'sessionAuth', 'after' => 'role:ADMIN|SUPERADMIN'] , 'prefix' => ''  ],  function () {                
            Route::get('customers','Operations\Users\User@retrieve');
            Route::get('customers/get/{id}','Operations\Users\User@customerProfile');
            Route::post('customer/update','Operations\Users\User@updateCustomerProfile')->name('customer.update');

            Route::get('suvery/get/{id}','Operations\Survey\Survey@index');
        
            Route::get('drivers/all','Operations\Drivers\Drivers@retrieve');
            Route::get('drivers/get/{id}','Operations\Drivers\Drivers@retrieveSingle');
            Route::get('drivers/getAvailable/{bookingId}','Operations\Drivers\Drivers@retrieveAvailable');
            Route::post('driver/create','Operations\Drivers\Drivers@create');
            Route::post('driver/edit/{id}','Operations\Drivers\Drivers@edit');
            Route::get('driver/delete/{id}','Operations\Drivers\Drivers@delete');
         
            Route::get('rides/history/{driverId}','Operations\Bookings\Bookings@retrieveHistory');
         
            Route::get('bookings/pending','Operations\Bookings\PendingBookings@retrieve');
            Route::get('bookings/all','Operations\Bookings\Bookings@retrieve');
            Route::get('bookings/complete','Operations\Bookings\ConfirmedBookings@retrieve');
            Route::get('bookings/cancelled','Operations\Bookings\CancelledBookings@retrieve');
            Route::get('bookings/rejected','Operations\Bookings\RejectedBookings@retrieve');
            Route::get('bookings/completed','Operations\Bookings\Bookings@retrieveCompleted');
            Route::post('bookings/assign/{bookingId}','Operations\Bookings\Bookings@assignDriver');
            
            Route::get('dashboard','Operations\Dashboard@showDashboard');
            Route::get('payments/online','Operations\Payments\Payment@retrieveOnline');
            Route::get('payments/bank','Operations\Payments\Payment@retrieveBank');
            Route::get('refunds','Operations\Payments\Payment@getRefunds');
            Route::get('confirm/refund/{refundId}','Operations\Payments\Payment@confirmRefund');

            Route::get('accept/booking/{bookingId}','Operations\Bookings\PendingBookings@acceptBooking');
            Route::get('complete/booking/{bookingId}','Operations\Bookings\ConfirmedBookings@completeBooking');
            Route::get('cancel/booking/{bookingId}','Operations\Bookings\ConfirmedBookings@cancelBooking');
            Route::get('reject/booking/{bookingId}','Operations\Bookings\PendingBookings@rejectBooking');
            Route::get('confirm/booking/{bookingId}','Operations\Bookings\PendingBookings@confirmBooking');

            Route::get('settings','Operations\PlatformSettings\PlatformSettingsController@index')->name('platform.settings');
            Route::post('settings/car-count','Operations\PlatformSettings\PlatformSettingsController@updateCarCount');
            Route::post('settings/pricing','Operations\PlatformSettings\PlatformSettingsController@updatePricing');
            Route::post('settings/password','Operations\PlatformSettings\PlatformSettingsController@updatePassword');
            Route::get('reports','Operations\Reports\ReportController@index');
         
   });

    Route::group(['middleware' => [ 'before' => 'sessionAuth', 'after' => 'role:SUPERADMIN|'] , 'prefix' => ''  ],  function () {
    	      Route::get('user-management','Operations\Users\User@retrieveAdmins');
              Route::get('delete/{id}','Operations\Users\User@removeAdmin');
              Route::post('edit/{id}','Operations\Users\User@editAdmin');
              Route::post('create','Operations\Users\User@createAdmin');
    });

   
});