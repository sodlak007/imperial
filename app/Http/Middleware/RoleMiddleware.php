<?php

namespace App\Http\Middleware;
 
use Illuminate\Support\Facades\Redirect;

use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        
        //verify role
        $role_list = explode("|", $role);
       
        if(count($role_list) == 0)
          $role_list = [$role];

         
         if(!in_array(\Auth::user()->role,$role_list)) {
            return Redirect::to('/');
        }


        return $next($request);
    }
}
