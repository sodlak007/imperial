<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Redirect;

use Closure;

class SessionAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //verify session here
        //print_r(\Auth::user()); die();


        if(!\Auth::check()){
          return Redirect::to('/admin/login?goto='.\Request::fullUrl())->with('error_msg','You need to login');
        }

        if(\Auth::user()->isActive == 0){
          return Redirect::to('/admin/login?goto='.\Request::fullUrl())->with('error_msg','Please contact admin');
        }

        return $next($request);
    }
}
