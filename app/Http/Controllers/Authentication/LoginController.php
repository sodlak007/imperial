<?php

namespace App\Http\Controllers\Authentication;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Models\Users\User;
use App\Http\Models\Resets\Reset;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Controllers\Services\Emails\Base as BaseEmail;
use App\Http\Controllers\Responses\AuthenticationResponse as AuthResponse;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function showAdminLogin(){
        return view('Admin.login');
    }

    public function showResetNotify(){
        return view('Admin.resetNotification');
    }

    public function showSuccessNotify(){
        return view('Admin.password-success');
    }

    public function logout(){
         \Auth::logout();
         return Redirect::to('/admin/login')->with('message','You have been successfully logged out.');
    }

    public function showReset(){
        $token = $_GET['token'];

        if($token == ""){
            return Redirect::to('/admin/login')->with('message','Reset Token is invalid, Please click on forgot Password to reset');
        }
        else{
            //display form
            $Reset = Reset::findToken($token);
        
            if( ($Reset == null) || (count($Reset) == 0) ){
                return Redirect::to('/admin/login')->with('message','Reset Token is invalid, Please click on forgot Password to reset');
            }

            $min_diff = Carbon::parse($Reset->created_at)->diffInSeconds(Carbon::parse(Carbon::now()), false);
            
            if($min_diff > 1800){
                return Redirect::to('/admin/login')->with('message','Reset Token has expired, Please click on forgot Password to reset');
            }

            return view('Admin.reset');
        }
    }

    public function doReset(Request $request)
    {
        $input = $request->all();
        $authResponse = new AuthResponse(206,"Unable to Proceed");
        
        $Reset = Reset::findToken($request->get('token'));

        
        if( ($Reset == null) || (count($Reset) == 0) ){
            $authResponse->setExtraMessage("The reset token is invalid");
            return $authResponse->getJsonResponse();
        }

        if($Reset->active == 0){
            $authResponse->setExtraMessage("Reset Token has been used");
            return $authResponse->getJsonResponse();
        }

        $min_diff = Carbon::parse($Reset->created_at)->diffInSeconds(Carbon::parse(Carbon::now()), false);

        if($min_diff > 1800){
            $authResponse->setExtraMessage("Reset Token has expired");
            return $authResponse->getJsonResponse();
        }

        //proceed to updating password
        
        try{
                \DB::table('users')
                    ->where('email', $Reset->email)
                    ->update(['password' => bcrypt($request->get('password'))]);
                
                \DB::table('password_resets')
                    ->where('token', $request->get('token'))
                    ->update(['active' => 0]);

                $authResponse->setCode(200);
                $authResponse->setMessage("Reset Successful");
                $authResponse->setExtraMessage("");     

                return $authResponse->getJsonResponse();
            }
            catch(\Exception $ex)
            {
                $authResponse->setCode(500);
                $authResponse->setMessage("An Error Occurred");
                $authResponse->setExtraMessage("An Eroor Occured");

                return $authResponse->getJsonResponse();
            }
    }

    public function adminRecover($email,$name){
         //create reset
        $Reset = new Reset();
        $Reset->email = $email;
        $Reset->token = str_random(60);
        $Reset->save();

        //send reset
        $data = ['message' => url('/admin/reset?token='.$Reset->token) ,'name' => $name];

        \Mail::to($email)->send(new BaseEmail('Imperial Admin', 'Emails.admin-reset', $data));
    }

    public function recover(Request $request)
    {
         $input = $request->all();
         $authResponse = new AuthResponse(206,"Unable to Proceed");

        //get the user role
        $user = User::findEmail($request->get('email'));

        if(count($user) == 0){
            $authResponse->setExtraMessage("User does not exist");
            return $authResponse->getJsonResponse();
        }

        if($user->role == 'CUSTOMER'){
            $authResponse->setExtraMessage("Unauthorized user");
            return $authResponse->getJsonResponse();      
        }

        if($user->isActive != 1){
              $authResponse->setExtraMessage("You have been removed from the Imperial database, please contact admin");
              return $authResponse->getJsonResponse();
        }

        //create reset
        $Reset = new Reset();
        $Reset->email = $request->get('email');
        $Reset->token = str_random(60);
        $Reset->save();

        //send reset
        $data = ['message' => url('/admin/reset?token='.$Reset->token) ,'name' => $user->name];

        \Mail::to($user->email)->send(new BaseEmail('Password Reset', 'Emails.reset', $data));

        $authResponse->setCode(200);
        $authResponse->setMessage("Password information has been sent to email address");
        $authResponse->setExtraMessage("");
        return $authResponse->getJsonResponse();
    }
    
    public function login(Request $request)
    {
        $input = $request->all();
        $authResponse = new AuthResponse(206,"Unable to Login");

       //get the user role
        $user = User::findEmail($request->get('email'));;

        if(count($user) == 0){
                $authResponse->setExtraMessage("User does not exist");
                return $authResponse->getJsonResponse();
        }

        if($user->isActive != 1){
              $authResponse->setExtraMessage("User has been removed");
                return $authResponse->getJsonResponse();
        }

        $credentials = array(
              'email' => $request->get('email'),
              'password' => $request->get('password')
        );

        

        if (\Auth::attempt($credentials)) {
            //create 
            //\Session::put('user', \Auth::user());

            $authResponse->setCode(200);
            $authResponse->setMessage("Login Successful");
            $authResponse->setExtraMessage("");
            return $authResponse->getJsonResponse();
        }
        else{
            $authResponse->setExtraMessage("Email or password incorrect");
            return $authResponse->getJsonResponse(); 
        }

       
    	
    }
    
}
