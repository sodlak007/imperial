<?php

namespace App\Http\Controllers\Responses;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Response;


class AuthenticationResponse extends BaseResponse
{
    private $extraMessage = "";

    public function __construct($code , $message){
        parent::__construct($code, $message);
        // Do stuff specific for extraMessage
    }

    public function setExtraMessage($message){
        $this->extraMessage = $message;
    }

    public function getExtraMessage(){
        return $this->extraMessage;
    }
   
    public function getJsonResponse(){
        $response = json_encode(array('message'=>parent::getMessage(),'extraMessage'=>$this->extraMessage));
        return Response($response, parent::getcode());
    }
}