<?php

namespace App\Http\Controllers\Responses;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Response;

class BaseResponse extends Controller
{
    private $code;
    private $message; 

    function __construct($code,$message) {
       $this->message = $message;
       $this->code = $code;
    }

    function setCode($code){
        $this->code = $code;
    }

    function getCode(){
        return $this->code;
    }

    function setMessage($message){
        $this->message = $message;
    }

    function getMessage(){
        return $this->message;
    }

    function getJsonResponse(){
        $response = json_encode(array('message'=>$this->message));
        return Response($response, $this->code);
    }

}