<?php

namespace App\Http\Controllers\Services\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Base extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $view;
    public $data;

    public function __construct($subject,$view,$data)
    {
        $this->subject = $subject;
        $this->view = $view;
        $this->data = $data;
    }

    public function build()
    {
       return $this->view($this->view)
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject($this->subject)
            ->with($this->data);
    }
}