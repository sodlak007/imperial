<?php

namespace App\Http\Controllers\Operations\Drivers;

use Illuminate\Http\Request;
use App\Http\Models\Bookings\Booking;
use App\Http\Models\Users\User;
use App\Http\Models\Drivers\Driver;
use App\Http\Controllers\Controller;
use App\Http\Models\Payments\Payment as PaymentDet;
use App\Http\Controllers\Operations\Users\User as UserController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Responses\DriversResponse;

use App\Http\Controllers\Services\Emails\Base as BaseEmail;

class Drivers extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Booking Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function retrieve(){
         $View = \View::make('Admin.drivers-all');  
        
         $drivers = Driver::getAllDrivers(1,($_GET['page']-1)*20);
         $total = Driver::count('isActive',1);

         $View->with('drivers',$drivers);
         $View->with('total',$total);;

         return $View;    
    }

    public function retrieveAvailable($bookingId){
         $driverResponse = new DriversResponse(200,url('/admin/bookings/assign/'.$bookingId.'/'));
        
         $booking  = Booking::find($bookingId);

         //get bookings for that date
         $sameDayBookings = Booking::getIDsOfBookingsOn($booking->date);
         $driverIds = [];

         foreach ($sameDayBookings as $booking) {
             if(@$booking->driverId != null){
                 array_push($driverIds, $booking->driverId);
             }
         }

         $drivers = Driver::getAvailableDrivers($driverIds);

         $driverResponse->setExtraMessage(json_encode($drivers)); 
         return $driverResponse->getJsonResponse();
    }

    public function delete($id){
         $driverResponse = new DriversResponse(200,"Success");
      
         $driver = Driver::find($id);
         $driver->isActive = 0;
         $driver->save();

         return $driverResponse->getJsonResponse();
    }

     public function retrieveSingle($id){
         $driverResponse = new DriversResponse(200,url('/admin/driver/edit/'));
         $View = \View::make('Admin.drivers-all');  
        
         $driver = Driver::find($id);
         $driverResponse->setExtraMessage(json_encode($driver)); 
         return $driverResponse->getJsonResponse();
    }

    public function create(Request $request){
        //check if there is not driver with that email
        $driverResponse = new DriversResponse(206,"Unable to Proceed");

        $driverExistsCount = Driver::count('email', $request->get('driverEmail'));
        
        if($driverExistsCount > 0){
            $driverResponse->setMessage("Driver with provided Email Exists Already");
            return $driverResponse->getJsonResponse(); 
        }

        $target = "0000";

        if(isset($_FILES['imageToUpload']) && ($_FILES["imageToUpload"]["name"] != '')){
           $target_dir = "uploads/drivers/";
           $target = str_replace(' ','', time().basename($_FILES["imageToUpload"]["name"]));
           $target_file = $target_dir . str_replace(' ','',$target);
           $uploadOk = 1;
           $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            // Check if image file is a actual image or fake image
            $check = getimagesize($_FILES["imageToUpload"]["tmp_name"]);
            if($check !== false) {
                $uploadOk = 1;
            } else {
               $driverResponse->setMessage("File is not an image");
               return $driverResponse->getJsonResponse(); 
            }
            // Check if file already exists
            if (file_exists($target_file)) {
                echo 'exists';
                $uploadOk = 0;
            }
            // Check file size
            if ($_FILES["imageToUpload"]["size"] > 500000) {
                $driverResponse->setMessage("File is too large");
                return $driverResponse->getJsonResponse(); 
            }

            //check size
            $image_info = getimagesize($_FILES["imageToUpload"]["tmp_name"]);
            $image_width = $image_info[0];
            $image_height = $image_info[1];

            //echo $image_height;

            /*if($image_height == 450 && $image_width == 750){}
            else{
                  $upload_message = "Image size should be 500 * 500.";
                  \Session::flash('message', $upload_message);
                  return redirect()->back(); 
            }*/

            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                  $driverResponse->setMessage("File should be jpg, png or gif");
                  return $driverResponse->getJsonResponse(); 
            }

            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                 $driverResponse->setMessage("Driver image could not be uploaded");
                 return $driverResponse->getJsonResponse();    
            } else {
                if (move_uploaded_file($_FILES["imageToUpload"]["tmp_name"], $target_file)) {
                } else {
                  $driverResponse->setMessage("Error uploading Driver Image");
                  return $driverResponse->getJsonResponse(); 
               }
            }
       }
       else{
            $driverResponse->setMessage("Please upload Driver Image");
            return $driverResponse->getJsonResponse();    
       }

       
        $Driver = new Driver();
        $Driver->firstname = $request->get('driverFirstName');
        $Driver->lastname = $request->get('driverLastName');
        $Driver->email = $request->get('driverEmail');
        $Driver->phone = $request->get('driverPhone');
        $Driver->photo = $target;
        $Driver->isActive = 1;
        $Driver->save();

        $driverResponse->setCode(200);
        $driverResponse->setMessage("Driver Added Successfully");
        $driverResponse->setExtraMessage(""); 

       return $driverResponse->getJsonResponse();
    }

    public function edit(Request $request, $id){
        //check if there is not driver with that email
        $driverResponse = new DriversResponse(206,"Unable to Proceed");
        try{
             $target = "0000";

            if(isset($_FILES['imageToUpload']) && ($_FILES["imageToUpload"]["name"] != '')){
            $target_dir = "uploads/drivers/";
            $target = str_replace(' ','', time().basename($_FILES["imageToUpload"]["name"]));
            $target_file = $target_dir . str_replace(' ','',$target);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
                // Check if image file is a actual image or fake image
                $check = getimagesize($_FILES["imageToUpload"]["tmp_name"]);
                if($check !== false) {
                    $uploadOk = 1;
                } else {
                $driverResponse->setMessage("File is not an image");
                return $driverResponse->getJsonResponse(); 
                }
                // Check if file already exists
                if (file_exists($target_file)) {
                    echo 'exists';
                    $uploadOk = 0;
                }
                // Check file size
                if ($_FILES["imageToUpload"]["size"] > 500000) {
                    $driverResponse->setMessage("File is too large");
                    return $driverResponse->getJsonResponse(); 
                }

                //check size
                $image_info = getimagesize($_FILES["imageToUpload"]["tmp_name"]);
                $image_width = $image_info[0];
                $image_height = $image_info[1];

                //echo $image_height;

                /*if($image_height == 450 && $image_width == 750){}
                else{
                    $upload_message = "Image size should be 500 * 500.";
                    \Session::flash('message', $upload_message);
                    return redirect()->back(); 
                }*/

                // Allow certain file formats
                if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif" ) {
                    $driverResponse->setMessage("File should be jpg, png or gif");
                    return $driverResponse->getJsonResponse(); 
                }

                // Check if $uploadOk is set to 0 by an error
                if ($uploadOk == 0) {
                    $driverResponse->setMessage("Driver image could not be uploaded");
                    return $driverResponse->getJsonResponse();    
                } else {
                    if (move_uploaded_file($_FILES["imageToUpload"]["tmp_name"], $target_file)) {
                    } else {
                    $driverResponse->setMessage("Error uploading Driver Image");
                    return $driverResponse->getJsonResponse(); 
                }
                }
            } 
            else{
                $driverResponse->setMessage("Please upload Driver Image");
                return $driverResponse->getJsonResponse();    
            }

        
            $Driver = Driver::find($id);
            $Driver->firstname = $request->get('driverFirstName');
            $Driver->lastname = $request->get('driverLastName');
            $Driver->phone = $request->get('driverPhone');
            $Driver->photo = $target;
            $Driver->isActive = 1;
            $Driver->save();

            $driverResponse->setCode(200);
            $driverResponse->setMessage("Driver Updated Successfully");
            $driverResponse->setExtraMessage(""); 

            return $driverResponse->getJsonResponse();
        }
        catch(\Exception $ex){
              $driverResponse->setMessage($ex->getMessage());
              return $driverResponse->getJsonResponse(); 
        }
       
    }
}