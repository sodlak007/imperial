<?php

namespace App\Http\Controllers\Operations;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Models\Rides\Ride;
use App\Http\Models\Payments\Payment;
use App\Http\Models\Bookings\Booking;
use App\Http\Models\Resets\Reset;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Controllers\Services\Emails\Base as BaseEmail;
use App\Http\Controllers\Responses\AuthenticationResponse as AuthResponse;

class Dashboard extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Dashboard Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }


    public function showDashboard(){
    
       $View = \View::make('Admin.dashboard');

       //get count of available cars for the month
       $totalCarsAvailable = Ride::getCount();

       if($totalCarsAvailable == null || (count($totalCarsAvailable) == 0) )
          $qty = 4;
       else
          $qty = $totalCarsAvailable->quantityInStock;

       //get count of bookings in the month
       $totalBookingsThisDay = Booking::countDayBookings();

       $availableCarsForBooking = $qty - $totalBookingsThisDay;
       $View->with('availableCarsForBooking',$availableCarsForBooking);

       //get bookings today
       $totalBookingsToday = Booking::countCurrentDayBookings();
       $View->with('bookingsToday',$totalBookingsToday);

       //get bookings today
       $totalPendingBookings = Booking::count('REQUESTED',0);
       $View->with('pendingBookings', $totalPendingBookings);

       //get today's earnings
       $totalEarningsToday = Payment::getTodayEarnings(); 
       $View->with('totalEarningsToday', $totalEarningsToday);

       //get last 10 bookings
       $recentPendingBookings = Booking::getAllBookings('REQUESTED',0);
       $View->with('recentPendingBookings',$recentPendingBookings);;

       return $View;
       
    }

   
}
