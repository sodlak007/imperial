<?php

namespace App\Http\Controllers\Operations\Payments;

use Illuminate\Http\Request;
use App\Http\Models\Bookings\Booking;
use App\Http\Models\Users\User;
use App\Http\Models\Refunds\Refund;
use App\Http\Models\Payments\Payment as PaymentDet;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Responses\BookingsResponse;
use App\Http\Controllers\Services\Emails\Base as BaseEmail;

class Payment extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

   
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public static function confirmRefund($refundId){
        $bookingResponse = new BookingsResponse(206,"Unable to Proceed");
        $refund = Refund::find($refundId);
        $customer = User::find($refund->user);

        \DB::table('refunds')
            ->where('id', $refundId)
            ->update(['status' => 'PAID']);

        $bookingResponse->setCode(200);
        $bookingResponse->setMessage("Refund Completed Successfully");
        $bookingResponse->setExtraMessage(""); 

         if($refund->hasFine == 1){
            $refundPrice = env('PRICE') - (10 / 100 * env('PRICE'));
        }
        else{
            $refundPrice = env('PRICE');
        }

        //nofity user
        $data = ['name' => $refund->accountName, 'amount' => number_format($refundPrice), 'accountNo' => $refund->accountNo, 'bank' => $refund->bank];
        \Mail::to($customer->email)->send(new BaseEmail('Refund Completion', 'Emails.refund-notification', $data));      
    
        return $bookingResponse->getJsonResponse();;
    }

     public static function getRefunds(){
         $View = \View::make('Admin.refunds');  
        
         $refunds = Refund::getAllRefunds(1,($_GET['page']-1)*20);
         $total = Refund::getCount(1);

         $View->with('refunds',$refunds);
         $View->with('total',$total);;

         return $View;    
    }

    public function retrieveBank(){ 
        $payments = PaymentDet::getPaymentsDetails(($_GET['page']-1)*20);

        $total = PaymentDet::getCount();
        
        $View = \View::make('Admin.bankPayments');    
        $View->with('data',$payments);
        $View->with('total',$total);
       
        return $View;    
    }

    public function retrieveOnline(){
            //pull payment from paystack
            $result = array();
            //Set other parameters as keys in the $postdata array
            $url = "https://api.paystack.co/transaction?perPage=30&status=success&page=".$_GET['page'];

            // create curl resource 
            $ch = curl_init(); 

            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Bearer '.env('PAYSTACK_SECRET')
                ));

            // set url 
            curl_setopt($ch, CURLOPT_URL, $url); 

            //return the transfer as a string 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

            // $output contains the output string 
            $output = json_decode(curl_exec($ch)); 

            //dd($output);

            // close curl resource to free up system resources 
            curl_close($ch);   
            
            $View = \View::make('Admin.onlinePayments');    
            $View->with('data',$output->data);
            $View->with('total',$output->meta->total);
       
            return $View;    
    }

   
}