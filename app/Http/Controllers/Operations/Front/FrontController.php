<?php

namespace App\Http\Controllers\Operations\Front;

use Illuminate\Http\Request;
use App\Http\Models\Bookings\Booking;
use App\Http\Models\Users\User;
use App\Http\Models\Payments\Payment as PaymentDet;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Responses\BookingsResponse;
use App\Http\Controllers\Services\Emails\Base as BaseEmail;

class FrontController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

   
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function home(){ 
        $View = \View::make('Website.home');    
       
        return $View;    
    }

}