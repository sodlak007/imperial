<?php

namespace App\Http\Controllers\Operations\PlatformSettings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Rides\Ride;
use App\User;
use Auth;
use Validator;

class PlatformSettingsController extends Controller
{
    public function index()
    {
    	$cars = Ride::find(1);

        $result = $cars;

        return view('Admin.settings')->with('car', $result);
    }

    public function updateCarCount(Request $request)
    {
    	$cars = Ride::find(1);

    	$cars->quantityInStock = $request->input('car');

    	$cars->save();

    	session()->flash('car', 'Cars in fleet updated');

    	return redirect()->back();
    }

	public function updatePricing(Request $request)
    {
		$key = "PRICE";
    	$value = $request->input('price');
		$path = base_path('.env');

		if(is_bool(env($key)))
		{
			$old = env($key)? 'true' : 'false';
		}
		elseif(env($key)===null){
			$old = 'null';
		}
		else{
			$old = env($key);
		}

		if (file_exists($path)) {
			file_put_contents($path, str_replace(
				"$key=".$old, "$key=".$value, file_get_contents($path)
			));
		}

    	session()->flash('pricing', 'Pricing updated');

    	return redirect()->back();
    }

    public function updatePassword(Request $request)
    {
    	$user = Auth::user();
		
		if(strlen($request->input('password')) < 6){
			session()->flash('error', 'Passwords too short');
                return redirect()->back();
		}

		$validator = Validator::make($request->all(), [
    		'password' => 'required|min:6|confirmed',
    	]);

    	if ($validator->fails()) {
               	session()->flash('error', 'Passwords do not match.');
                return redirect()->back();
         }

    	$result = User::find($user->id);

    	$result->password = bcrypt($request->input('password'));

    	$result->save();

    	session()->flash('password', 'Password updated');

    	return redirect()->intended('/admin/settings');
    }
}
