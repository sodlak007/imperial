<?php

namespace App\Http\Controllers\Operations\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Users\User;
use App\Http\Models\Payments\Payment;

class ReportController extends Controller
{
    public function index()
    {
		 //do filtering by ride type, keyword, date range
		 $transactions = Payment::getPaymentsData(($_GET['page']-1)*20, @$_GET['bookingStatus'], @$_GET['keyword'], @$_GET['start'], @$_GET['end']);
		 $total = Payment::getPaymentsDataCount( @$_GET['bookingStatus'], @$_GET['keyword'], @$_GET['start'], @$_GET['end']);
		 
		 $filter = "";

		 if(@$_GET['bookingStatus']){
			 $filter .= '&bookingStatus='.$_GET['bookingStatus'];
		 }

		 if(@$_GET['keyword']){
			 $filter .= '&keyword='.$_GET['keyword'];
		 }

		 if(@$_GET['start'] && @$_GET['end']){
			 $filter .= '&start='.$_GET['start'].'&end='.$_GET['end'];
		 }

         return view('Admin.reports')
		   ->with('total',$total)
		   ->with('base', '/admin/reports?page='.$_GET['page'])
		   ->with('filter',$filter)
    	   ->with('transactions', $transactions);
    }
}
