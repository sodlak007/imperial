<?php

namespace App\Http\Controllers\Operations\Survey;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\Survey\Survey;

class SurveyController extends Controller
{
    public function index($id)
    {
    	$data['id'] = $id;

    	return view('Website.survey')->with('id', $data);
    }

    public function store(Request $request)
    {
    	$bookindId = $_POST['booking_id'];

        //dd(implode(':', $request->input('question_2')));

    	$survey =  new Survey();

    	$survey->booking_id = $bookindId;
    	$survey->question_1 = $request->input('question_1');
    	$survey->question_2 = implode(", ", $request->input('question_2'));
    	$survey->question_3 = implode(", ", $request->input('question_3'));
    	$survey->comment = $request->input('feedback');

    	$survey->save();

    	return view('Website.survey-success');

    }
}
