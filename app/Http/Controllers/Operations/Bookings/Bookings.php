<?php

namespace App\Http\Controllers\Operations\Bookings;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Models\Bookings\Booking;
use App\Http\Models\Users\User;
use App\Http\Models\Drivers\Driver;
use App\Http\Models\Rides\Ride;
use App\Http\Controllers\Controller;
use App\Http\Models\Payments\Payment as PaymentDet;
use App\Http\Controllers\Operations\Users\User as UserController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Responses\BookingsResponse;

use App\Http\Controllers\Services\Emails\Base as BaseEmail;

class Bookings extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Booking Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function showSuccess(){
        $View = \View::make('Website.cancelSuccess');
        return $View;
    }

    public function cancelFromUser($rideID){
        $rideID=  base64_decode($rideID);
        $View = \View::make('Website.userCancel');
        $booking = Booking::find($rideID);
        $View->with('bookingMessage',"");
        $View->with('rideID',$rideID);

        //check duration
        if(Carbon::parse($booking->date)->startOfDay() == Carbon::now()->startOfDay()){
                $minute_diff = abs(Carbon::parse($booking->date . ' ' . $booking->time)->diffInMinutes(Carbon::parse(Carbon::now()), false));
                if($minute_diff <= 60){
                     $View->with('bookingMessage',"You cannot cancel ride 1 hour or less to the ride");
                     return $View;
                }
         }
        
        if($booking->approvalStatus == 'CANCELLED'){
             $View->with('bookingMessage',"Booking has already been Cancelled");
             return $View;
        }

        $date_diff = Carbon::parse($booking->accepted_at)->diffInDays(Carbon::parse(Carbon::now()), false);
        if($date_diff > 1){
            $View->with('fine_message',true);
        }
        else{
            $View->with('fine_message',false);
        }

        $View->with('booking',$booking);
        return $View;
    }

    public function retrieve(){
         $View = \View::make('Admin.bookings-all');  
        
         $recentRequestedBookings = Booking::getBookings('REQUESTED',($_GET['page']-1)*20);
         $total = Booking::count('REQUESTED',0);

         $View->with('recentRequestedBookings',$recentRequestedBookings);
         $View->with('total',$total);;

         return $View;    
    }

    public function retrieveCompleted(){
         $View = \View::make('Admin.completed-rides');  
        
         $completeRides = Booking::getBookings('COMPLETED',($_GET['page']-1)*20);
         $total = Booking::count('COMPLETED',0);

         $View->with('completeRides',$completeRides);
         $View->with('total',$total);;

         return $View;    
    }

    public function retrieveHistory($driverId){
         $View = \View::make('Admin.driver-history');
         $View->with('driverId',$driverId);  
        
         $driverHistory = Booking::getDriverHistory($driverId,($_GET['page']-1)*20);
         $total = Booking::countDriverHistory($driverId);

         $View->with('driverHistory',$driverHistory);
         $View->with('total',$total);;

         return $View;    
    }

    public function assignDriver(Request $request, $bookingId){
         $bookingResponse = new BookingsResponse(206,"Unable to Proceed");
         $booking = Booking::find($bookingId);
         $driver = Driver::find($request->get('driverId'));
         $customer = User::find($booking->user);

         $booking->driverId = $request->get('driverId');
         $booking->save();

         $bookingResponse->setCode(200);
         $bookingResponse->setMessage("Driver Assigned Successfully");
         $bookingResponse->setExtraMessage(""); 

         $data = [ 'driverName' => $driver->firstname, 'customer' => $customer->name, 'customerPhone' => $customer->phone, 'customerAddress' => $booking->pickUpAddress, 'bookingTime' => $booking->time, 'bookingDate' => $booking->date];
         \Mail::to($driver->email)->send(new BaseEmail('Ride Assignment Notification', 'Emails.driver-assignment-notification', $data));

        return $bookingResponse->getJsonResponse();
    }

    public function create(Request $request){
        $bookingResponse = new BookingsResponse(206,"Unable to Proceed");
        $userController = new UserController();
        
        $input = $request->all(); 
        $totalCarsAvailable = Ride::getCount();

        //check if a booking exist on date
        $existingCount = Booking::countExisting($request->get('pickUpDate'));

        if (!filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
             $bookingResponse->setMessage("Invalid email format");
            return $bookingResponse->getJsonResponse(); 
        }

        if(strlen($request->get('phone')) < 11){
            $bookingResponse->setMessage("Phone number must be 11 digits");
            return $bookingResponse->getJsonResponse(); 
        }

        if( $existingCount >= $totalCarsAvailable->quantityInStock){
              $bookingResponse->setMessage("No available cars for chosen date");
              return $bookingResponse->getJsonResponse();
        }
  

        $booking = new Booking();

        //confirm user existence
        $user = User::findEmail($request->get('email'));

        if(count($user) == 0){
            //create user
            $user = $userController->create($request);
        }

       $booking->user = $user->id;
       $booking->pickUpAddress = $request->get('pickUpAddress');
       $booking->rideId = 1;
       $booking->invoiceId = 0;
       $booking->paymentStatus = 'REQUESTED';
       $booking->approvalStatus = 'REQUESTED';
       $booking->date = $request->get('pickUpDate');
       $booking->time = $request->get('pickUpTime');
       $booking->save();

       
       $data = [ 'name' => $user->name,  'customer' => $user->name,   'bookingDate' => $booking->date , 'bookingTime' => $booking->time , 'customerPhone' =>$user->phone, 'customerAddress' => $booking->pickUpAddress ];
                  
       \Mail::to($user->email)->send(new BaseEmail('Imperial Booking Notification', 'Emails.booking', $data));

       //send email to admin
       //get admins
       $admins = User::getAdmins(0);

       foreach($admins as $admin){
           $data['adminName'] = $admin->name;
           \Mail::to($admin->email)->send(new BaseEmail('Imperial Booking Notification', 'Emails.admin-notify', $data));
       }


       $bookingResponse->setCode(200);
       $bookingResponse->setMessage("Booking Confirmed Successfully");
       $bookingResponse->setExtraMessage(""); 

       return $bookingResponse->getJsonResponse();
    }

}