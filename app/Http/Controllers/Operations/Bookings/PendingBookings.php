<?php

namespace App\Http\Controllers\Operations\Bookings;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Models\Bookings\Booking;
use App\Http\Models\Users\User;
use App\Http\Models\Payments\Payment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Responses\BookingsResponse;
use App\Http\Controllers\Services\Emails\Base as BaseEmail;

class PendingBookings extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function retrieve(){
        $View = \View::make('Admin.bookings');  
        
        
        $recentPendingBookings = Booking::getBookings('PENDING',($_GET['page']-1)*20);
        $total = Booking::count('PENDING',0);

        $View->with('recentPendingBookings',$recentPendingBookings);;
        $View->with('total',$total);;

        return $View;    
    }

    public function acceptBooking($bookingId){
        $bookingResponse = new BookingsResponse(206,"Unable to Proceed");
        
        try{
             \DB::table('bookings')
                    ->where('id', $bookingId)
                    ->update(['approvalStatus' => 'PENDING', 'accepted_at' => Carbon::now()]);

             //send a mail containing payment info to customer
             $booking  = Booking::find($bookingId);
             $customer = User::find($booking->user);

             $data = ['link' => env('LINK') , 'name' => $customer->name, 'date' => date("M jS, Y", strtotime($booking->date))];
             \Mail::to($customer->email)->send(new BaseEmail('Booking Accepted - Pending Payment', 'Emails.accept', $data));
             
             $bookingResponse->setCode(200);
             $bookingResponse->setMessage("Booking Accepted Successfully");
             $bookingResponse->setExtraMessage(""); 
             
             return $bookingResponse->getJsonResponse();;
        }
        catch(\Exception $ex){
             //echo $ex->getMessage();
             return $bookingResponse->getJsonResponse();;
        }
    }

    public function confirmBooking($bookingId){
        $bookingResponse = new BookingsResponse(206,"Unable to Proceed");
        
        try{
             \DB::table('bookings')
                    ->where('id', $bookingId)
                    ->update(['approvalStatus' => 'CONFIRMED', 'paymentStatus' => 'CONFIRMED']);

             //send a mail containing payment info to customer
             $booking  = Booking::find($bookingId);
             $customer = User::find($booking->user);

             $Payment = new Payment();
             $Payment->amount = 50000;
             $Payment->vat = 0;
             $Payment->userid= $customer->id;
             $Payment->bookingId = $booking->id;
             $Payment->status = 'success';
             

            if(isset($_GET['mode']) && ($_GET['mode'] == 'bank')){
                

                if(!isset($_GET['tref']) || ($_GET['tref'] == '')){
                    $bookingResponse->setMessage("Please provide transaction reference");
                    return $bookingResponse->getJsonResponse();
                }
                else{
                    $ref = $_GET['tref'];
                    $Payment->reference = $ref;
                    $Payment->channel = 'BANK';
                    $Payment->save();
                }
             }
             else{
                    $ref = $_GET['tref'];
                    $Payment->reference = $ref;
                    $Payment->channel = 'ONLINE';
                    $Payment->save();
             }
             

            $bookingResponse->setCode(200);
            $bookingResponse->setMessage("Booking Created Successfully");
            $bookingResponse->setExtraMessage(""); 

             $data = ['name' => $customer->name, 'date' => date("M jS, Y", strtotime($booking->date)), 'cancelLink' => url('/').'/ride/cancel/'.base64_encode($booking->id) ];
             \Mail::to($customer->email)->send(new BaseEmail('Booking Approved', 'Emails.confirm', $data));
             
             return $bookingResponse->getJsonResponse();
        }
        catch(\Exception $ex){
            $bookingResponse->setMessage($ex->getMessage());
            return $bookingResponse->getJsonResponse();;
        }
    }
        
      public function rejectBooking($bookingId){
        $bookingResponse = new BookingsResponse(206,"Unable to Proceed");
        
        try{
             \DB::table('bookings')
                    ->where('id', $bookingId)
                    ->update(['approvalStatus' => 'REJECTED']);

             //send a mail containing payment info to customer
             $booking  = Booking::find($bookingId);
             $customer = User::find($booking->user);

            $data = ['link' => env('LINK') , 'name' => $customer->name, 'date' => date("M jS, Y", strtotime($booking->created_at))];
            \Mail::to($customer->email)->send(new BaseEmail('Booked Ride Cancelled', 'Emails.reject', $data));
             
             $bookingResponse->setCode(200);
             $bookingResponse->setMessage("Booking Rejected Successfully");
             $bookingResponse->setExtraMessage(""); 
             
             return $bookingResponse->getJsonResponse();;
        }
        catch(\Exception $ex){
            echo $ex->getMessage();
            return $bookingResponse->getJsonResponse();;
        }
    }  
}