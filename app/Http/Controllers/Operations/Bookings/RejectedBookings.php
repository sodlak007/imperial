<?php

namespace App\Http\Controllers\Operations\Bookings;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Models\Bookings\Booking;
use App\Http\Models\Users\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Responses\BookingsResponse;
use App\Http\Controllers\Services\Emails\Base as BaseEmail;

class RejectedBookings extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     * 
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function retrieve(){
         $View = \View::make('Admin.bookings-rejected');  
        
        $rejectedBookings = Booking::getBookings('REJECTED',($_GET['page']-1)*20);
        $total = Booking::count('REJECTED',0);

        $View->with('rejectedBookings',$rejectedBookings);;
        $View->with('total',$total);;

        return $View;    
    }

   
}