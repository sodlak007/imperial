<?php

namespace App\Http\Controllers\Operations\Bookings;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Models\Bookings\Booking;
use App\Http\Models\Users\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Models\Drivers\Driver;
use App\Http\Models\Refunds\Refund;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Responses\BookingsResponse;
use App\Http\Controllers\Services\Emails\Base as BaseEmail;
use App\Http\Models\Payments\Payment as PaymentDet;

class ConfirmedBookings extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     * 
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function logCancelRequest(Request $request, $bookingId){
        $bookingResponse = new BookingsResponse(206,"Unable to Proceed");
        $booking  = Booking::find($bookingId);
        $customer = User::find($booking->user); 

        if($request->get('bank') == 'Select'){
            $bookingResponse->setMessage("Please Choose a Bank");
            return $bookingResponse->getJsonResponse(); 
        }

          
        
        //Include Booking In Refunds
        $Refund = new Refund();
        $Refund->bookingId = $bookingId;
        $Refund->user = $booking->user;
        $Refund->hasFine = $request->get('hasFine');
        $Refund->status = 'PENDING';
        $Refund->isActive = 1;
        $Refund->accountNo = $request->get('accNumber');
        $Refund->accountName = $request->get('accHolder');
        $Refund->bank = $request->get('bank');
        $Refund->reason = $request->get('reason');
        $Refund->save();

        if($request->get('hasFine') == 1){
            $refundPrice = env('PRICE') - (10 / 100 * env('PRICE'));
        }
        else{
            $refundPrice = env('PRICE');
        }

        //SEND MAIL TO ADMIN AND USER
        $data = ['name' => $customer->name, 'amount' => number_format($refundPrice), 'accountNo' => $request->get('accNumber'), 'bank' => $request->get('bank')];
        \Mail::to($customer->email)->send(new BaseEmail('Cancel Ride', 'Emails.cancel-request', $data));      

        $admins = User::getAdmins(0);

       foreach($admins as $admin){
           $data = ['name' => $request->get('accHolder'), 'rideId' => $bookingId, 'amount' => number_format($refundPrice), 'accountNo' => $request->get('accNumber'), 'bank' => $request->get('bank')];
            \Mail::to($admin->email)->send(new BaseEmail('Ride Cancel Request for '.$customer->name, 'Emails.admin-cancel-request', $data));      
       }
       
       
        //cancel ride
        
        //cancel the ride
        \DB::table('bookings')
            ->where('id', $bookingId)
            ->update(['approvalStatus' => 'CANCELLED']);
      
        $bookingResponse->setCode(200);
        $bookingResponse->setMessage("Booking Cancelled Successfully");
        $bookingResponse->setExtraMessage(""); 
        
        return $bookingResponse->getJsonResponse();;
    }

    public function retrieve(){
        $View = \View::make('Admin.completeBookings');  
        
        $completeBookings = Booking::getBookings('CONFIRMED',($_GET['page']-1)*20);
        $total = Booking::count('CONFIRMED',0);

        $View->with('confirmedBookings',$completeBookings);;
        $View->with('total',$total);;

        return $View;    
    }

    public function completeBooking($bookingId){
        $booking  = Booking::find($bookingId);
        $bookingResponse = new BookingsResponse(206,"Unable to Proceed");

        \DB::table('bookings')
            ->where('id', $bookingId)
            ->update(['approvalStatus' => 'COMPLETED']);

        $customer = User::find($booking->user); 

        $bookingResponse->setCode(200);
        $bookingResponse->setMessage("Booking Completed Successfully");
        $bookingResponse->setExtraMessage(""); 

        //send email with survey link
         $data = ['name' => $customer->name, 'link' => 'ride/survey/'.$bookingId];
         \Mail::to($customer->email)->send(new BaseEmail('Ride Completed - Rate our Service', 'Emails.survey', $data));      
        
        return $bookingResponse->getJsonResponse();;
    }

    public function cancelBooking($bookingId){
        $bookingResponse = new BookingsResponse(206,"Unable to Proceed");

         $booking  = Booking::find($bookingId);

         \DB::table('bookings')
            ->where('id', $bookingId)
            ->update(['approvalStatus' => 'CANCELLED']);
        
        $customer = User::find($booking->user);     
       
        if($booking->approvalStatus == 'PENDING'){
            $bookingResponse->setMessage("Booking Cancelled Successfully and customer has been notified, 10% fine does not apply"); 
            //send normal notification
            $data = ['name' => $customer->name, 'date' => date("M jS, Y", strtotime($booking->date))];
            \Mail::to($customer->email)->send(new BaseEmail('Booking Accepted - Pending Payment', 'Emails.cancel', $data));      
            
        }
        else{
            //get payment date
            $payment = PaymentDet::getBookingPayment($booking->id);
             //check time the booking was accepted
            $date_diff = Carbon::parse($payment->created_at)->diffInDays(Carbon::parse(Carbon::now()), false);

            if($date_diff < 1){
                 $bookingResponse->setMessage("Booking Cancelled Successfully and customer has been notified, 10% fine does not apply"); 
                 //send normal notification
                 $data = ['name' => $customer->name, 'date' => date("M jS, Y", strtotime($booking->date))];
                 \Mail::to($customer->email)->send(new BaseEmail('Booking Accepted - Pending Payment', 'Emails.cancel', $data));    
            }
            else{
                  $bookingResponse->setMessage("Booking Cancelled Successfully and customer has been notified, Booking was confirmed over 24 hours ago so 10% fine applies");
                 //send 10% fine notification
                  $data = [ 'name' => $customer->name, 'date' => date("M jS, Y", strtotime($booking->date))];
                 \Mail::to($customer->email)->send(new BaseEmail('Booking Accepted - Pending Payment', 'Emails.cancel-fine', $data));
            }

        }

        $bookingResponse->setCode(200);
        $bookingResponse->setExtraMessage(""); 
        
        return $bookingResponse->getJsonResponse();;
    }
}