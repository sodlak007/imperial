<?php

namespace App\Http\Controllers\Operations\Users;

use Illuminate\Http\Request;
use App\Http\Models\Bookings\Booking;
use App\Http\Models\Users\User as UserDao;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Authentication\LoginController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Responses\BaseResponse;
use App\Http\Controllers\Services\Emails\Base as BaseEmail;
use App\Http\Models\Drivers\Driver;
use App\Http\Models\Survey\Survey;

class User extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Booking Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->loginController = new LoginController();
    }

    public function retrieve(){
        $users = UserDao::getUsers(($_GET['page']-1)*20);

        $total = UserDao::countUsers();
        
        $View = \View::make('Admin.customers');    
        $View->with('data',$users);
        $View->with('total',$total);
       
        return $View;    
    }

    public function customerProfile($id)
    {
        $user = UserDao::find($id);
        $bookings =  \DB::table('bookings')->where('user', $id)->get();
        $last_10_bookings = Booking::where('user', $id)->orderBy('id', 'desc')->take(10)->get();
        //dd($last_10_bookings);
        $surveys = array();

        foreach ($last_10_bookings as $booking) {
            // dd($booking->id);
            $survey = Survey::where('booking_id', $booking->id)->first();

            $driver = Driver::find($booking->driverId);

            $result['survey'] = $survey;
            $result['driver'] = $driver;

            array_push($surveys, $result);
        }
        //dd($surveys);

        return view('Admin.customer-profile')
                ->with('user', $user)
                ->with('bookings', $bookings)
                ->with('surveys', $surveys);
    }

    public function updateCustomerProfile(Request $request)
    {
        $id = $_POST['id'];

        $user = UserDao::find($id);

        if($request->input('email') != ''){
            $user->email = $request->input('email');
        }

        if($request->input('phone') != ''){
            $user->phone = $request->input('phone');
        }

        $user->save();

        return redirect()->back();
    }

    public function retrieveAdmins(){
        $admins = UserDao::getAdmins(($_GET['page']-1)*20);

        $total = UserDao::countAdmins();
        
        $View = \View::make('Admin.user-management');    
        $View->with('data',$admins);
        $View->with('total',$total);
       
        return $View;    
    }

    public function removeAdmin($id){
        \DB::table('users')
            ->where('id', $id)
            ->update(['isActive' => 0]);

        $response = new BaseResponse(200, 'Successful');
        return $response->getJsonResponse();
    }

    public function editAdmin(Request $request, $id){
        \DB::table('users')
            ->where('id', $id)
            ->update(['role' => $request->get('role')]);

        $response = new BaseResponse(200, 'Successful');
        return $response->getJsonResponse();
    }

    public function createAdmin(Request $request){
        $user = UserDao::findEmail($request->get('email'));

        if(count($user) == 0){
            //create user
            $user = new UserDao();
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->role = $request->get('role');
            $user->phone = '08012345687';
            $user->password = bcrypt($request->get('role'));
            $user->save();

            //create password reset
            $this->loginController->adminRecover($request->get('email'), $request->get('name'));

             $response = new BaseResponse(200, 'Success');
            return $response->getJsonResponse();
        }
        else{
            if($user->role == 'CUSTOMER')
               $response = new BaseResponse(206, 'Email Exists as a customer email already');
            else
               $response = new BaseResponse(206, 'Email Exists as a administrator email already');
        
            return $response->getJsonResponse();
        }
    }


    public function create(Request $request){
        
        $user = new UserDao();

        //confirm user existence
        if($request->get('fullname') == null){
            $user->name = $request->get('firstname') . ' '. $request->get('lastname');
        }
        else
            $user->name = $request->get('fullname');

        $user->email = $request->get('email');
        $user->phone = $request->get('phone');
        $user->role = 'CUSTOMER';
        $user->password = bcrypt($request->get('phone'));
        $user->isActive = 1;
        $user->save();

        return $user;
    }

}