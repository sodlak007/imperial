<?php

namespace App\Http\Controllers\Operations;

use Carbon\Carbon; 
use Illuminate\Http\Request;
use App\Http\Models\Rides\Ride;
use App\Http\Models\Payments\Payment;
use App\Http\Models\Bookings\Booking;
use App\Http\Models\Resets\Reset;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Http\Models\Users\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Controllers\Services\Emails\Base as BaseEmail;
use App\Http\Controllers\Responses\AuthenticationResponse as AuthResponse;

class CronController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Dashboard Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function doDriver24HourNotify(){
        //get all confirmed bookings
        $completeBookings = Booking::getBookingsAll('CONFIRMED');
        foreach($completeBookings as $complete){
            //compare date to now
            $day_diff = Carbon::parse(Carbon::now()->startOfDay())->diffInDays(Carbon::parse($complete->date)->startOfDay() , false);
             if($complete->driver_first != "")
                if($day_diff == 1){
                    //send a notification mail to driver
                    $data = [ 'driverName' => $complete->driver_first, 'customer' => $complete->name, 'customerPhone' => $complete->phone, 'customerAddress' => $complete->pickUpAddress, 'bookingTime' => $complete->time, 'bookingDate' => $complete->date];
                    \Mail::to($complete->driver_email)->send(new BaseEmail('Ride Reminder for '.date("F j, Y", strtotime($complete->date)), 'Emails.driver-24-reminder-mail', $data));

                    //send a notification mail to rider
                    $data = [ 'riderName' => $complete->name,  'bookingDate' => $complete->date , 'cancelLink' => url('/').'/ride/cancel/'.base64_encode($complete->id)];
                    \Mail::to($complete->email)->send(new BaseEmail('Ride Reminder for '.date("F j, Y", strtotime($complete->date)), 'Emails.rider-24-reminder-mail', $data));

                }
        }
    }

    public function doDriver1HourNotify(){
        //get all confirmed bookings
        $completeBookings = Booking::getBookingsAll('CONFIRMED');
        foreach($completeBookings as $complete){
            if(Carbon::parse($complete->date)->startOfDay()->diffInMinutes(Carbon::now()->startOfDay()) == 0 ){
                $minute_diff = Carbon::parse(Carbon::now())->diffInMinutes(Carbon::parse($complete->date . ' ' . $complete->time), false);
                if($minute_diff >= 0 && $minute_diff <= 60){
                    //send a notification mail to rider
                    $data = ['riderName' => $complete->name, 'driverName' => $complete->driver_first . ' ' . $complete->driver_last, 'driverPhone' => $complete->driverPhone, 'customerPhone' => $complete->phone, 'bookingTime' => $complete->time, 'photo' => env('DRIVER_UPLOADS').$complete->photo, 'bookingDate' => $complete->date];
                    \Mail::to($complete->email)->send(new BaseEmail('Ride Reminder for '.date("F j, Y", strtotime($complete->date)), 'Emails.rider-one-hour-reminder', $data));

                    //send a notification mail to driver
                    $data = [ 'riderName' => $complete->name, 'customerAddress' => $complete->pickUpAddress,  'driverName' => $complete->driver_first . ' ' . $complete->driver_last, 'riderPhone' => $complete->phone,  'bookingDate' => $complete->date , 'bookingTime' => $complete->time];
                    \Mail::to($complete->driver_email)->send(new BaseEmail('Ride Reminder for '.date("F j, Y", strtotime($complete->date)), 'Emails.driver-one-hour-reminder', $data));
                }
            }
          
        }
    }

    public function freeUpRides(){
    
      //run a cron on all pending bookings, look for those greater than 1 hour and set to rejected
      $bookings = Booking::getAllPending();
      foreach($bookings as $booking){
          //get booking in case status has changed
          $specBooking = Booking::find($booking->id);
          $customer = User::find($booking->user);

          if($specBooking->approvalStatus == 'PENDING'){
            $hour_diff = Carbon::parse($specBooking->accepted_at)->diffInHours(Carbon::parse(Carbon::now()), false);
            if($hour_diff >= 1){
                //send a mail
                $data = ['name' => $customer->name];
                \Mail::to($customer->email)->send(new BaseEmail('Booking Cancelled - Delayed Payment', 'Emails.delayed-pay', $data));

                $specBooking->approvalStatus = 'CANCELLED';
                $specBooking->save();
            }

          }
      }
       
    }

   
}
