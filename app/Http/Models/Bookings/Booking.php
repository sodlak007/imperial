<?php
namespace App\Http\Models\Bookings;

use App\Http\Models\BaseModel;
use Carbon\Carbon;

class Booking extends BaseModel {
	protected $table = 'bookings';
    //protected $guarded = array('id');
    //protected $fillable = ['uid','pleskid','amount','plan','span','planid','domain','duration','purpose','paid','status','invoiceID','paymentDate','paymentRef','nextDueDate','finalDueDate','renewalsLeft','processed'];
               
	
    public function __construct()
    {
       parent::__construct('bookings');
    }

    public static function getAllBookings($status,$skip){
        $data = \DB::table("bookings as b")
             ->join('users as u','u.id','=','b.user')
             ->select('b.id as id', 'b.user', 'b.pickUpAddress', 'b.created_at', 'u.name' , 'u.email', 'u.phone' , 'b.date' , 'b.time')
             ->where('approvalStatus','=',$status)
             ->orderBy('created_at', 'desc')
             ->take(20)
             ->get();

        return $data;
    }

    public static function getAllPending(){
        $data = \DB::table("bookings as b")
             ->join('users as u','u.id','=','b.user')
             ->select('b.id as id', 'b.user', 'b.pickUpAddress', 'b.created_at', 'u.name' , 'u.email', 'u.phone' , 'b.date' , 'b.time')
             ->where('approvalStatus','=','PENDING')
             ->orderBy('created_at', 'desc')
             ->get();

        return $data;
    }

    public static function getBookings($status,$skip){
        $data = \DB::table("bookings as b")
             ->join('users as u','u.id','=','b.user')
             ->leftJoin('drivers as d', 'd.id','=','b.driverId')
             ->skip($skip)
             ->select('b.id as id', 'b.user', 'b.pickUpAddress', 'b.created_at', 'u.name' , 'u.email', 'u.phone', 'b.date' , 'd.firstname as driver_first' , 'd.lastname as driver_last' , 'b.time')
             ->where('approvalStatus','=',$status)
             ->orderBy('created_at', 'desc')
             ->take(20)
             ->get();

        return $data;
    }

    public static function getBookingsAll($status){
        $data = \DB::table("bookings as b")
             ->join('users as u','u.id','=','b.user')
             ->leftJoin('drivers as d', 'd.id','=','b.driverId')
             ->select('b.id as id', 'b.user', 'b.pickUpAddress', 'b.created_at', 'u.name' , 'u.email', 'u.phone', 'b.date' , 'd.email as driver_email', 'd.firstname as driver_first' , 'd.lastname as driver_last' , 'd.photo', 'd.phone as driverPhone',  'b.time')
             ->where('approvalStatus','=',$status)
             ->orderBy('created_at', 'desc')
             ->get();

        return $data;
    }

    public static function getDriverHistory($driverId,$skip){
           $data = \DB::table("bookings as b")
             ->join('users as u','u.id','=','b.user')
             ->select('b.id as id', 'b.user', 'b.pickUpAddress', 'b.created_at', 'u.name' , 'b.dropOffTime', 'u.email', 'u.phone', 'b.date' , 'b.time')
             ->skip($skip)
             ->where('driverId','=',$driverId)
             ->orderBy('created_at', 'desc')
             ->take(20)
             ->get();

        return $data;
    }


    public static function count($status,$skip){
        return parent::count('bookings',['where'=>['approvalStatus'=>$status]]);
    }

    public static function countExisting($date){
        $currentMonth = date('m');
        $data = \DB::table("bookings")
            ->where('date', '=' , Carbon::parse($date)->toDateString())
            ->wherein('approvalStatus',['PENDING','CONFIRMED'])
            ->count();

        return $data;
    }

    public static function countDriverHistory($driverId){
        $data = \DB::table("bookings")
            ->where('driverId','=',$driverId)
            ->count();

        return $data;
    }

    public static function getIDsOfBookingsOn($date){
        $data = \DB::table("bookings")
            ->where('date', '=' , Carbon::parse($date)->toDateString())
            ->get();


        return $data;
    }

    public static function countDayBookings(){
        $currentMonth = date('m');
        $data = \DB::table("bookings")
            ->where('date', '=', Carbon::today()->toDateString())
            ->wherein('approvalStatus',['PENDING','CONFIRMED'])
            ->count();

        return $data;
    }

     public static function countCurrentDayBookings(){
        $data = \DB::table("bookings")
            ->where('approvalStatus','=','REQUESTED')
            ->where('created_at', '>=', Carbon::today())
            ->count();

        return $data;
    }
}