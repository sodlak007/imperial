<?php
namespace App\Http\Models\Drivers;

use App\Http\Models\BaseModel;
use Carbon\Carbon;

class Driver extends BaseModel {
	protected $table = 'drivers';
    //protected $guarded = array('id');
    //protected $fillable = ['uid','pleskid','amount','plan','span','planid','domain','duration','purpose','paid','status','invoiceID','paymentDate','paymentRef','nextDueDate','finalDueDate','renewalsLeft','processed'];
               
	
    public function __construct()
    {
       parent::__construct('drivers');
    }

    public static function getAllDrivers($status,$skip){
        $data = \DB::table("drivers as d")
             ->select('d.id as id', 'd.firstname', 'd.lastname', 'd.email', 'd.phone', 'd.photo')
             ->where('isActive','=',$status)
             ->skip($skip)
             ->orderBy('d.created_at', 'asc')
             ->take(20)
             ->get();

        return $data;
    }

    public static function getAvailableDrivers($driverIds){
         $data = \DB::table("drivers as d")
             ->select('d.id as id', 'd.firstname', 'd.lastname', 'd.photo')
             ->where('isActive',1)
             ->whereNotIn('d.id',$driverIds)
             ->orderBy('d.created_at', 'desc')
             ->get();

        return $data;
    }

    public static function getFreeDrivers($date){
         $data = \DB::table("drivers as d")
             ->select('d.id as id', 'd.firstname', 'd.lastname', 'd.email', 'd.phone', 'd.photo')
             ->where('isActive','=',$status)
             ->skip($skip)
             ->orderBy('created_at', 'desc')
             ->take(20)
             ->get();

        return $data;
    }

    public static function count($criteria, $status){
        return parent::count("drivers",['where'=>[$criteria=>$status]]);
    }
}