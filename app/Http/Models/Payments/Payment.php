<?php
namespace App\Http\Models\Payments;

use App\Http\Models\BaseModel;
use App\Http\Refunds\Refund;
use Carbon\Carbon;

class Payment extends BaseModel {
	protected $table = 'payments';
    //protected $guarded = array('id');
    //protected $fillable = ['uid','pleskid','amount','plan','span','planid','domain','duration','purpose','paid','status','invoiceID','paymentDate','paymentRef','nextDueDate','finalDueDate','renewalsLeft','processed'];
               
	
    public function __construct()
    {
       parent::__construct('payments');
    }

    public static function getPayments($status,$skip){
        return parent::find('payments',['where'=>['paymentStatus'=>$status]]);
    }

    public static function confirmRefund($refundId){
        
    }

     public static function getBookingPayment($booking_id){
         $data = \DB::table("payments as p")
             ->where('bookingId','=',$booking_id)
             ->first();

        return $data;
     }
    
    public static function getCount(){
         $result = \DB::table('payments') 
                 ->where('channel','=','BANK')
                 ->count();

          return $result;
    }

    public static function count($status,$skip){
        return parent::count('payments',['where'=>['paymentStatus'=>$status]]);
    }

    public static function getTodayEarnings(){
        return \DB::table('payments')->where('created_at', '>=', Carbon::today()->startOfDay())->where('status','=','success')->sum('amount');
    }

    public static function getPaymentsDataCount($status, $keyword, $start, $end){
         $data = \DB::table("payments as p")
             ->join('users as u','p.userid','=','u.id')
             ->select('p.id as id', 'p.amount' , 'p.created_at' , 'p.reference' , 'u.name' , 'u.email', 'u.phone');
            

         if($status != "" && $status != null){
            $data = $data->join('bookings as b','b.id','=','p.bookingId')->where('b.approvalStatus','=',$status);
         }

         if($start != "" && $start != null && $end != "" && $end != null){
            $data = $data->whereBetween('p.created_at', [$start, $end]);
         }

         if($keyword != "" && $keyword != null){
            $data = $data->where('u.name', 'like', '%' . $keyword . '%')->orWhere('u.email', 'like', '%' . $keyword . '%')->orWhere('u.phone', 'like', '%' . $keyword . '%')->orWhere('p.reference', 'like', '%' . $keyword. '%');
         }

         $result  = $data->count();

         return $result;
     }

    public static function getPaymentsData($skip, $status, $keyword, $start, $end){
         $data = \DB::table("payments as p")
             ->join('users as u','p.userid','=','u.id')
             ->select('p.id as id', 'p.amount' , 'p.created_at' , 'p.reference' , 'u.name' , 'u.email', 'u.phone');
            

         if($status != "" && $status != null){
            $data = $data->join('bookings as b','b.id','=','p.bookingId')->where('b.approvalStatus','=',$status);
         }

         if($start != "" && $start != null && $end != "" && $end != null){
            $data = $data->whereBetween('p.created_at', [$start, $end]);
         }

         if($keyword != "" && $keyword != null){
            $data = $data->where('u.name', 'like', '%' . $keyword . '%')->orWhere('u.email', 'like', '%' . $keyword . '%')->orWhere('u.phone', 'like', '%' . $keyword . '%')->orWhere('p.reference', 'like', '%' . $keyword. '%');
          }

         $result  = $data->skip($skip)->orderBy('p.created_at', 'desc')->take(20)->get();

        return $result;
     }
    
     public static function getPaymentsDetails($skip){
         $data = \DB::table("payments as p")
             ->join('users as u','p.userid','=','u.id')
             ->select('p.id as id', 'p.amount' , 'p.created_at' , 'p.reference' , 'u.name' , 'u.email', 'u.phone')
             ->skip($skip)
             ->where('channel','=','BANK')
             ->orderBy('created_at', 'desc')
             ->take(20)
             ->get();

        return $data;
     }
}