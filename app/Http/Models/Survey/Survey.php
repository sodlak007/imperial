<?php
namespace App\Http\Models\Survey;

use App\Http\Models\BaseModel;
use Carbon\Carbon;

class Survey extends BaseModel {
	protected $table = 'survey';               
	
    public function __construct()
    {
       parent::__construct('survey');
    }

}