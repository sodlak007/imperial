<?php
namespace App\Http\Models\Rides;

use App\Http\Models\BaseModel;

class Ride extends BaseModel {
	protected $table = 'rides';
    //protected $guarded = array('id');
    //protected $fillable = ['uid','pleskid','amount','plan','span','planid','domain','duration','purpose','paid','status','invoiceID','paymentDate','paymentRef','nextDueDate','finalDueDate','renewalsLeft','processed'];    
	
    public function __construct()
    {
       parent::__construct('rides');
    }

    public static function getRides($status,$skip){
        return parent::find('rides',['where'=>['status'=>$status]],$skip);
    }

    public static function getAvailableLuxuryCars(){
        return parent::count('rides', ['where'=>['category'=>'Luxury']]);
    }

    public static function getCount(){
         $result = \DB::table('rides')
                   ->first();

          return $result;
    }
}