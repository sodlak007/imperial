<?php
namespace App\Http\Models\Refunds;

use App\Http\Models\BaseModel;
use Carbon\Carbon;

class Refund extends BaseModel {
	protected $table = 'refunds';
    //protected $guarded = array('id');
    //protected $fillable = ['uid','pleskid','amount','plan','span','planid','domain','duration','purpose','paid','status','invoiceID','paymentDate','paymentRef','nextDueDate','finalDueDate','renewalsLeft','processed'];
               
	
    public function __construct()
    {
       parent::__construct('refunds');
    }

    public static function getAllRefunds($status, $skip){
        $data = \DB::table("refunds as d")
             ->select('d.id as id', 'd.hasFine', 'd.finePercentage', 'd.status',  'd.bank','d.accountNo','d.accountName','u.name', 'b.id as rideId')
             ->join('users as u','u.id','=','d.user')
             ->join('bookings as b','b.id','=','d.bookingId')
             ->where('d. isActive','=',$status)
             ->skip($skip)
             ->orderBy('d.created_at', 'desc')
             ->take(20)
             ->get();

        return $data;
    }

     public static function getCount($status){
         $result = \DB::table('refunds') 
                   ->where('isActive','=',$status)
                   ->count();

          return $result;
    }
   
}