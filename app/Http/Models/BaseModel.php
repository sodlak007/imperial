<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model {
	protected $table = '';
    //protected $guarded = array('id');
    //protected $fillable = ['uid','pleskid','amount','plan','span','planid','domain','duration','purpose','paid','status','invoiceID','paymentDate','paymentRef','nextDueDate','finalDueDate','renewalsLeft','processed'];
               
	
    public function __construct($table)
    {
       $this->table = $table;
    }

    public static function get($table,$parameters,$skip = 0 ){
        $entity = \DB::table($table);
        foreach($parameters as $queryOption => $queryValues){
            if($queryOption == 'where'){
                foreach($queryValues as $key => $value){
                    $entity->where($key,'=',$value);
                }
            }
        }

        return $entity->orderBy('created_at', 'desc')->skip($skip)->paginate(10);
    }

    public static function count($table,$parameters){
        $entity = \DB::table($table);
        foreach($parameters as $queryOption => $queryValues){
            if($queryOption == 'where'){
                foreach($queryValues as $key => $value){
                    $entity->where($key,'=',$value);
                }
            }
        }

        return $entity->count();
    }

    public static function findOne($table, $parameters){
        $entity = \DB::table($table);
        foreach($parameters as $queryOption => $queryValues){
            if($queryOption == 'where'){
                foreach($queryValues as $key => $value){
                    $entity->where($key,'=',$value);
                }
            }
        }

        return $entity->first();
    }

}
