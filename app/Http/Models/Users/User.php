<?php
namespace App\Http\Models\Users;

use App\Http\Models\BaseModel;

class User extends BaseModel {
	protected $table = 'users';
    //protected $guarded = array('id');
    //protected $fillable = ['uid','pleskid','amount','plan','span','planid','domain','duration','purpose','paid','status','invoiceID','paymentDate','paymentRef','nextDueDate','finalDueDate','renewalsLeft','processed'];
               
	
    public function __construct()
    {
       parent::__construct('users');
    }

    public static function findEmail($email){
        return parent::findOne('users',['where'=>['email'=>$email]]);
    }

    public static function getUsers($skip){
        $data = \DB::table("users as u")
             ->select('u.id', 'u.name' , 'u.email', 'u.phone')
             ->skip($skip)
             ->where('role','=','CUSTOMER')
             ->where('isActive','=',1)
             ->orderBy('created_at', 'desc')
             ->take(20)
             ->get();

        return $data;
    }

     public static function countUsers(){
        $data = \DB::table("users")
             ->where('role','=','CUSTOMER')
             ->count();

        return $data;
    }

     public static function getAdmins($skip){
        $data = \DB::table("users as u")
             ->select('u.id', 'u.name' , 'u.email', 'u.role')
             ->skip($skip)
             ->where('role','!=','CUSTOMER')
             ->where('isActive','=',1)
             ->orderBy('created_at', 'desc')
             ->take(20)
             ->get();

        return $data;
    }

     public static function countAdmins(){
        $data = \DB::table("users")
             ->where('role','!=','CUSTOMER')
             ->count();

        return $data;
    }
}