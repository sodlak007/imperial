<?php
namespace App\Http\Models\Resets;

use App\Http\Models\BaseModel;

class Reset extends BaseModel {
	protected $table = 'password_resets';
    //protected $guarded = array('id');
    //protected $fillable = ['uid','pleskid','amount','plan','span','planid','domain','duration','purpose','paid','status','invoiceID','paymentDate','paymentRef','nextDueDate','finalDueDate','renewalsLeft','processed'];
               
	
    public function __construct()
    {
       parent::__construct('password_resets');
    }

    public static function findToken($token){
        return parent::findOne('password_resets',['where'=>['token'=>$token]]);
    }
}