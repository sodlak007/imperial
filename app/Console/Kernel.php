<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\drive24HourNotify::class,
        Commands\freeUpRides::class,
        Commands\hourToRide::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
       $schedule->command('drive:24HourNotify')
               ->timezone('Africa/Lagos')->dailyAt('10:00');
     
       $schedule->command('rides:freeUp')
               ->timezone('Africa/Lagos')->dailyAt('10:00'); 
       

       $schedule->command('drive:1HourNotify')
               ->timezone('Africa/Lagos')->hourly()->between('2:15','23:15');   
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */

    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
