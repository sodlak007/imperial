<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Operations\CronController as Cron;
use Carbon\Carbon;

class drive24HourNotify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'drive:24HourNotify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify Driver of a ride 24 hours to the ride';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cron = new Cron();
        $cron->doDriver24HourNotify();

        $this->info('Reminders Have Been Sent');
    }
}
