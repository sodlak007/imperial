<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Operations\CronController as Cron;

class hourToRide extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'drive:1HourNotify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify Driver and Rider 1 hour to ride';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cron = new Cron();
        $cron->doDriver1HourNotify();

        $this->info('Reminders Have Been Sent');
    }
}
