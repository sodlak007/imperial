<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Operations\CronController as Cron;

class freeUpRides extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rides:freeUp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Free up reserved rides that are not paid for on time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cron = new Cron();
        $cron->freeUpRides();

        $this->info('Rides have been freed up');
    }
}
