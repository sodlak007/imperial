<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Bookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('bookings'))     
            Schema::create('bookings', function (Blueprint $table) {
                $table->increments('id');
                $table->string('user');
                $table->string('pickUpAddress');
                $table->string('rideId');
                $table->string('paymentStatus');
                $table->string('approvalStatus');
                $table->string('invoiceId');
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
