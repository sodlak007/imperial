<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Invoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('invoices'))
         Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bookingId');
            $table->string('amount');
            $table->string('promoCode');
            $table->string('discount');
            $table->string('status');
            $table->string('vat');
            $table->string('transactionId');
            $table->string('paymentChannel');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
