<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Survey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('survey'))
            Schema::create('survey', function (Blueprint $table) {
                $table->increments('id');
                $table->string('booking_id');
                $table->string('question_1')->nullable();
                $table->string('question_2')->nullable();
                $table->string('question_3')->nullable();
                $table->string('comment')->nullable();
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('users');
    }
}
