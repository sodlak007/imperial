<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Payments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if (!Schema::hasTable('payments'))
            Schema::create('payments', function (Blueprint $table) {
                $table->increments('id');
                $table->string('amount');
                $table->string('vat');
                $table->string('channel');
                $table->integer('userid');
                $table->integer('bookingId');
                $table->string('status');
                $table->rememberToken();
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
