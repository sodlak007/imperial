<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Refunds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         if (!Schema::hasTable('refunds'))
            Schema::create('refunds', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user');
                $table->integer('bookingId');
                $table->integer('hasFine');
                $table->integer('finePercentage')->default(10);
                $table->string('status');
                $table->string('accountNo');
                $table->string('accountName');
                $table->string('bank');
                $table->string('reason');
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
