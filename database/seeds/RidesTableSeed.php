<?php

use Illuminate\Database\Seeder;

class RidesTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rides')->insert([
            'category' => 'Luxury',
            'brand' => 'Chrysler',
            'quantityInStock' => 4,
            'created_at' => '2018-02-25 19:08:41'
        ]);
    }
}
