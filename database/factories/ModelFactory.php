<?php

use Faker\Generator as Faker;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Http\Models\Users\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'phone' => $faker->phoneNumber,
        'remember_token' => str_random(10),
        'role' => 'SUPERADMIN'
    ];
});

$factory->define(\App\Http\Models\Rides\Ride::class, function (Faker $faker) {
    return [
        'category' => 'Luxury',
        'brand' => 	'Chrysler',
        'quantityInStock' => '3'
    ];
});
